onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic /quadratic/iclk
add wave -noupdate -format Literal /quadratic/ix
add wave -noupdate -format Literal /quadratic/oy
add wave -noupdate -format Literal /quadratic/svalue_ax
add wave -noupdate -format Literal /quadratic/svalue_bx
add wave -noupdate -format Literal /quadratic/svalue_axx
add wave -noupdate -format Literal /quadratic/svalue_bxpc
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1197 ns} 0}
configure wave -namecolwidth 150
configure wave -valuecolwidth 40
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {1694 ns}
