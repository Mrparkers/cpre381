library IEEE;
use IEEE.std_logic_1164.all;

entity Einstein is

  port(iCLK             	    : in std_logic;
       iM 		            : in integer;
       oE 		            : out integer);

end Einstein;

architecture structure of Einstein is
  
  -- Describe the component entities as defined in Adder.vhd 
  -- and Multiplier.vhd (not strictly necessary).
  component Adder
    port(iCLK           : in std_logic;
         iA             : in integer;
         iB             : in integer;
         oC             : out integer);
  end component;

  component Multiplier
    port(iCLK           : in std_logic;
         iA             : in integer;
         iB             : in integer;
         oC             : out integer);
  end component;

  -- Constants for the value of c
  constant cC : integer := 9487;

  -- Signals to store c*c
  signal sVALUE_CC : integer;

begin

  
  ---------------------------------------------------------------------------
  -- Level 1: Calculate C*C
  ---------------------------------------------------------------------------
  g_Mult1: Multiplier
    port MAP(iCLK             => iCLK,
             iA               => cC,
             iB               => cC,
             oC               => sVALUE_CC);
    
 ---------------------------------------------------------------------------
  -- Level 2: Calculate m*C*C
  ---------------------------------------------------------------------------
  g_Mult2: Multiplier
    port MAP(iCLK             => iCLK,
             iA               => sVALUE_CC,
             iB               => iM,
             oC               => oE);

  
end structure;


