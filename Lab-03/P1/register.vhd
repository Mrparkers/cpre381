library IEEE;
use IEEE.std_logic_1164.all;

entity registerNBit is
    generic(N : integer := 32);
    port(i_CLK        : in std_logic;     -- Clock input
         i_RST        : in std_logic;     -- Reset input
         i_WE         : in std_logic;     -- Write enable input
         i_D          : in std_logic_vector(N-1 downto 0);     -- Data value input
         o_Q          : out std_logic_vector(N-1 downto 0));   -- Data value output

end registerNBit;

architecture mixed of registerNBit is

    component dff
        port(i_CLK        : in std_logic;     -- Clock input
             i_RST        : in std_logic;     -- Reset input
             i_WE         : in std_logic;     -- Write enable input
             i_D          : in std_logic;     -- Data value input
             o_Q          : out std_logic);   -- Data value output
    end component;

begin
    -- Make d flip flops
    dff_gen: for i in 0 to N-1 generate
        dff_i: dff
            port map(i_clk => i_clk,
                     i_rst => i_rst,
                     i_we => i_we,
                     i_d => i_d(i),
                     o_q => o_q(i));
    end generate;

    process(i_clk, i_rst, i_we)
    begin
        if (i_RST = '1' and i_we = '1') then
                o_Q <= (others => '0');
        elsif (rising_edge(i_CLK) and i_we = '1') then
                o_Q <= i_D;
        end if;
    end process;

  
end mixed;
