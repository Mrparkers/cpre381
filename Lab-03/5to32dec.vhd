library IEEE;
use IEEE.std_logic_1164.all;

entity fiveToThirtyTwoDec is
    port(input : in std_logic_vector(4 downto 0);
         output : out std_logic_vector(31 downto 0));

end fiveToThirtyTwoDec;

architecture mixed of fiveToThirtyTwoDec is

begin
    output <= x"00000001" when input = "00000" else
    x"00000002" when input = "00001" else
    x"00000004" when input = "00010" else
    x"00000008" when input = "00011" else
    x"00000010" when input = "00100" else
    x"00000020" when input = "00101" else
    x"00000040" when input = "00110" else
    x"00000080" when input = "00111" else
    x"00000100" when input = "01000" else
    x"00000200" when input = "01001" else
    x"00000400" when input = "01010" else
    x"00000800" when input = "01011" else
    x"00001000" when input = "01100" else
    x"00002000" when input = "01101" else
    x"00004000" when input = "01110" else
    x"00008000" when input = "01111" else
    x"00010000" when input = "10000" else
    x"00020000" when input = "10001" else
    x"00040000" when input = "10010" else
    x"00080000" when input = "10011" else
    x"00100000" when input = "10100" else
    x"00200000" when input = "10101" else
    x"00400000" when input = "10110" else
    x"00800000" when input = "10111" else
    x"01000000" when input = "11000" else
    x"02000000" when input = "11001" else
    x"04000000" when input = "11010" else
    x"08000000" when input = "11011" else
    x"10000000" when input = "11100" else
    x"20000000" when input = "11101" else
    x"40000000" when input = "11110" else
    x"80000000";
  
end mixed;
