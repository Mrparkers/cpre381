library IEEE;
use IEEE.std_logic_1164.all;
use work.utils.all;

entity register_file is
    port(i_clk: in std_logic;
         read_reg_1: in std_logic_vector(4 downto 0);
         read_reg_2: in std_logic_vector(4 downto 0);
         write_reg: in std_logic_vector(4 downto 0);
         write_data: in std_logic_vector(31 downto 0);
         reg_write: in std_logic;
         read_data_1: out std_logic_vector(31 downto 0);
         read_data_2: out std_logic_vector(31 downto 0));

end register_file;

architecture mixed of register_file is

    component thirtyTwoToOneMux
        port(sel: in std_logic_vector(4 downto 0);
             input: in array32_bit(31 downto 0);
             output: out std_logic_vector(31 downto 0));
    end component;

    component fiveToThirtyTwoDec
        port(input : in std_logic_vector(4 downto 0);
             output : out std_logic_vector(31 downto 0));
    end component;

    component registerNBit
         port(i_CLK        : in std_logic;     -- Clock input
              i_RST        : in std_logic;     -- Reset input
              i_WE         : in std_logic;     -- Write enable input
              i_D          : in std_logic_vector(31 downto 0);     -- Data value input
              o_Q          : out std_logic_vector(31 downto 0));   -- Data value output
    end component;

    signal temp: std_logic_vector(31 downto 0);
    signal write_enable_bits: std_logic_vector(31 downto 0);
    signal global_write_extended: std_logic_vector(31 downto 0);
    signal actual_write_enable_bits: std_logic_vector(31 downto 0);
    signal data: array32_bit(31 downto 0);

begin
    -- Get which register to write to
    dec1: fiveToThirtyTwoDec
    port MAP(input => write_reg,
             output => write_enable_bits);

    process(reg_write) begin
        global_write_extended <= (others => reg_write);
    end process;

    actual_write_enable_bits <= global_write_extended and write_enable_bits;

    mux1: thirtyTwoToOneMux
    port map(sel => read_reg_1,
             input => data,
             output => read_data_1);

    mux2: thirtyTwoToOneMux
    port map(sel => read_reg_2,
             input => data,
             output => read_data_2);

    -- Make registers
    reg_1: registerNBit
        port map(i_clk => i_clk,
                 i_rst => '1',
                 i_we => '1',
                 i_d => x"00000000",
                 o_q => data(0));

    reg_gen: for i in 1 to 31 generate
        reg_i: registerNBit
            port map(i_clk => i_clk,
                     i_rst => '0',
                     i_we => actual_write_enable_bits(i),
                     i_d => write_data,
                     o_q => data(i));
    end generate;

  
end mixed;

