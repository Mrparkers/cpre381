

library IEEE;
use IEEE.std_logic_1164.all;

entity mux2nBitDataflow is
  generic(N : integer := 8);
  port(i_A  : in std_logic_vector(N-1 downto 0);
       i_B  : in std_logic_vector(N-1 downto 0);
       i_C  : in std_logic_vector(N-1 downto 0);
       o_F  : out std_logic_vector(N-1 downto 0));

end mux2nBitDataflow;

architecture dataflow of mux2nBitDataflow is

begin

o_F <= ((not i_C) and i_A) or (i_C and i_B);
  
end dataflow;
