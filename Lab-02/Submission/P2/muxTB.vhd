

library IEEE;
use IEEE.std_logic_1164.all;

-- This is an empty entity so we don't need to declare ports
entity muxTB is
 port (i : in std_logic); -- Random port so the thing actually works
 --generic(N : integer := 8);
end muxTB;

architecture behavior of muxTB is

component mux2nBitDataflow
  generic(N : integer := 8);
  port(i_A  : in std_logic_vector(N-1 downto 0);
       i_B  : in std_logic_vector(N-1 downto 0);
       i_C  : in std_logic_vector(N-1 downto 0);
       o_F  : out std_logic_vector(N-1 downto 0));
end component;

component mux2nbit
  generic(N : integer := 8);
  port(i_A  : in std_logic_vector(N-1 downto 0);
       i_B  : in std_logic_vector(N-1 downto 0);
       i_C  : in std_logic_vector(N-1 downto 0);
       o_F  : out std_logic_vector(N-1 downto 0));
end component;

constant N : integer := 8;
signal s_A : std_logic_vector(N-1 downto 0);
signal s_B : std_logic_vector(N-1 downto 0);
signal s_C : std_logic_vector(N-1 downto 0);
signal s_D : std_logic_vector(N-1 downto 0);
signal s_E : std_logic_vector(N-1 downto 0);

begin

DUT: mux2nbit
  port map(i_A  => s_A,
  	        i_B  => s_B,
		i_C  => s_C,
  	        o_F  => s_D);

  process
  begin

    s_A <= b"01010101";
    s_B <= b"00110011";
    s_C <= b"00001111";
    wait for 100 ns;

  end process;

DUT2: mux2nBitDataflow
  port map(i_A  => s_A,
  	        i_B  => s_B,
		i_C  => s_C,
  	        o_F  => s_E);

  process
  begin

    s_A <= b"01010101";
    s_B <= b"00110011";
    s_C <= b"00001111";
    wait for 100 ns;

  end process;
  
end behavior;
