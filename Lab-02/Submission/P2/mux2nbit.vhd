


library IEEE;
use IEEE.std_logic_1164.all;

entity mux2nbit is
  generic(N : integer := 8);
  port(i_A  : in std_logic_vector(N-1 downto 0);
       i_B  : in std_logic_vector(N-1 downto 0);
       i_C  : in std_logic_vector(N-1 downto 0);
       o_F  : out std_logic_vector(N-1 downto 0));

end mux2nbit;

architecture structure of mux2nbit is

component mux2
  port(i_A  : in std_logic;
       i_B  : in std_logic;
       i_C  : in std_logic;
       o_F  : out std_logic);
end component;

begin

-- We loop through and instantiate and connect N and2 modules
G1: for i in 0 to N-1 generate
  mux_i: mux2 
    port map(i_A  => i_A(i),
	     i_B  => i_B(i),
	     i_C  => i_C(i),
  	     o_F  => o_F(i));
end generate;

  
end structure;
