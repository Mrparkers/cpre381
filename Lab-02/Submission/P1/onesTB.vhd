

library IEEE;
use IEEE.std_logic_1164.all;

-- This is an empty entity so we don't need to declare ports
entity onesTB is
 port (i : in std_logic); -- Random port so the thing actually works
 --generic(N : integer := 8);
end onesTB;

architecture behavior of onesTB is

component ones_complimenter_dataflow
  generic(N : integer := 8);
  port(i_A  : in std_logic_vector(N-1 downto 0);
       o_F  : out std_logic_vector(N-1 downto 0));
end component;

component ones_complimenter
  generic(N : integer := 8);
  port(i_A  : in std_logic_vector(N-1 downto 0);
       o_F  : out std_logic_vector(N-1 downto 0));
end component;

constant N : integer := 8;
signal s_A : std_logic_vector(N-1 downto 0);
signal s_B : std_logic_vector(N-1 downto 0);
signal s_C : std_logic_vector(N-1 downto 0);

begin

DUT: ones_complimenter
  port map(i_A  => s_A,
  	        o_F  => s_B);

  process
  begin

    s_A <= b"01010101";
    wait for 100 ns;

  end process;

DUT2: ones_complimenter_dataflow
  port map(i_A  => s_A,
  	        o_F  => s_C);

  process
  begin

    s_A <= b"01010101";
    wait for 100 ns;

  end process;
  
end behavior;
