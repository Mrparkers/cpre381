

library IEEE;
use IEEE.std_logic_1164.all;

entity addSub is
  generic(N : integer := 8);
  port(i_A     : in std_logic_vector(N-1 downto 0);
       i_B     : in std_logic_vector(N-1 downto 0);
       i_C     : in std_logic;
       o_Cout  : out std_logic;
       o_sum   : out std_logic_vector(N-1 downto 0));

end addSub;

architecture structure of addSub is

  component fullAdderNBit
	  generic(N : integer := 8);
	  port(i_A     : in std_logic_vector(N-1 downto 0);
	       i_B     : in std_logic_vector(N-1 downto 0);
	       i_Cin   : in std_logic;
	       o_Cout  : out std_logic;
	       o_sum   : out std_logic_vector(N-1 downto 0));
  end component;

  component ones_complimenter
	  generic(N : integer := 8);
	  port(i_A  : in std_logic_vector(N-1 downto 0);
	       o_F  : out std_logic_vector(N-1 downto 0));
  end component;

  component mux2nbit
	  generic(N : integer := 8);
	  port(i_A  : in std_logic_vector(N-1 downto 0);
	       i_B  : in std_logic_vector(N-1 downto 0);
	       i_C  : in std_logic_vector(N-1 downto 0);
	       o_F  : out std_logic_vector(N-1 downto 0));
  end component;

  signal notB, negB, muxControl, realB : std_logic_vector(N-1 downto 0);
  signal temp : std_logic;

begin
G1: for i in 0 to N-1 generate
  muxControl(i) <= i_C;
end generate;

  g_INV1: ones_complimenter
    port MAP(i_A => i_B,
	     o_F => notB);

  g_FA1: fullAdderNBit
    port MAP(i_A	=> notB,
	     i_B	=> b"00000001",
	     i_Cin => '0',
	     o_Cout	=> temp,
             o_sum => negB);

  g_MUX1: mux2nbit
    port MAP(i_A	=> i_B,
	     i_B	=> negB,
	     i_C => muxControl,
	     o_F	=> realB);

  g_FA2: fullAdderNBit
    port MAP(i_A	=> i_A,
	     i_B	=> realB,
	     i_Cin => '0',
	     o_Cout	=> o_Cout,
             o_sum => o_sum);
  
end structure;
