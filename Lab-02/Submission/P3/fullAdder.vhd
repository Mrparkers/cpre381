

library IEEE;
use IEEE.std_logic_1164.all;

entity fullAdder is

  port(i_A          : in std_logic;
       i_B	    : in std_logic;
       i_Cin        : in std_logic;
       o_Cout	    : out std_logic;
       o_sum        : out std_logic);

end fullAdder;

architecture structure of fullAdder is

  component and2
    port(i_A	: in std_logic;
         i_B	: in std_logic;
	 o_F	: out std_logic);
  end component;

  component or3
    port(i_A	: in std_logic;
         i_B	: in std_logic;
         i_C	: in std_logic;
	 o_F	: out std_logic);
  end component;

  component xor2
    port(i_A    : in std_logic;
         i_B	: in std_logic;
         o_F    : out std_logic);
  end component;

  signal AxorB, AandB, AandCin, BandCin : std_logic;

begin

  g_Xor1: xor2
    port MAP(i_A	=> i_A,
	     i_B	=> i_B,
	     o_F	=> AxorB);

  g_And1: and2
    port MAP(i_A	=> i_A,
	     i_B	=> i_B,
	     o_F	=> AandB);

  g_And2: and2
    port MAP(i_A	=> i_A,
	     i_B	=> i_Cin,
	     o_F	=> AandCin);

  g_And3: and2
    port MAP(i_A	=> i_B,
	     i_B	=> i_Cin,
	     o_F	=> BandCin);

  g_Xor2: xor2
    port MAP(i_A	=> AxorB,
	     i_B	=> i_Cin,
	     o_F	=> o_sum);

  g_Or1: or3
    port MAP(i_A	=> AandB,
	     i_B	=> AandCin,
	     i_C	=> BandCin,
	     o_F	=> o_Cout);
  
end structure;
