


library IEEE;
use IEEE.std_logic_1164.all;

entity fullAdderNBit is
  generic(N    : integer := 8);
  port(i_A     : in std_logic_vector(N-1 downto 0);
       i_B     : in std_logic_vector(N-1 downto 0);
       i_Cin   : in std_logic;
       o_Cout  : out std_logic;
       o_sum   : out std_logic_vector(N-1 downto 0));

end fullAdderNBit;

architecture structure of fullAdderNBit is

component fullAdder
  port(i_A     : in std_logic;
       i_B     : in std_logic;
       i_Cin   : in std_logic;
       o_Cout  : out std_logic;
       o_sum   : out std_logic);
end component;

--signal carry : std_logic := '0';
signal temp : std_logic_vector(N downto 0);

begin

temp(0) <= i_Cin;
o_Cout <= temp(N);


-- We loop through and instantiate and connect N and2 modules
G1: for i in 0 to N-1 generate
  fullAdder_i: fullAdder 
    port map(i_A     => i_A(i),
	     i_B     => i_B(i),
	     i_Cin   => temp(i),
  	     o_Cout  => temp(i+1),
  	     o_sum   => o_sum(i));
end generate;



  
end structure;
