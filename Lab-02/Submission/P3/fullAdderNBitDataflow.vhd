
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.std_logic_arith.all;

entity fullAdderNBitDataflow is
  generic(N    : integer := 8);
  port(i_A     : in std_logic_vector(N-1 downto 0);
       i_B     : in std_logic_vector(N-1 downto 0);
       i_Cin   : in std_logic;
       o_Cout  : out std_logic;
       o_sum   : out std_logic_vector(N-1 downto 0));

end fullAdderNBitDataflow;

architecture dataflow of fullAdderNBitDataflow is

signal temp : std_logic_vector(N downto 0);
signal temp2 : std_logic_vector(N downto 0);

begin

temp2 <= ('0' & i_A) + ('0' & i_B);
temp <= temp2 + i_Cin;
o_Cout <= temp(N);
o_Sum <= temp(N-1 downto 0);
  
end dataflow;
