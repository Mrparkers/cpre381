

library IEEE;
use IEEE.std_logic_1164.all;

entity mux2 is

  port(i_A          : in std_logic;
       i_B	    : in std_logic;
       i_C          : in std_logic;
       o_F          : out std_logic);

end mux2;

architecture structure of mux2 is

  component and2
    port(i_A	: in std_logic;
         i_B	: in std_logic;
	 o_F	: out std_logic);
  end component;

  component or2
    port(i_A	: in std_logic;
         i_B	: in std_logic;
	 o_F	: out std_logic);
  end component;

  component inv
    port(i_A    : in std_logic;
         o_F    : out std_logic);
  end component;

  signal BandC, notC, AandnotC : std_logic;

begin

  g_And1: and2
    port MAP(i_A	=> i_B,
	     i_B	=> i_C,
	     o_F	=> BandC);

  g_Not: inv
    port MAP(i_A	=> i_C,
	     o_F	=> notC);

  g_And2: and2
    port MAP(i_A	=> i_A,
	     i_B	=> notC,
	     o_F	=> AandnotC);

  g_Or: or2
    port MAP(i_A	=> BandC,
	     i_B	=> AandnotC,
	     o_F	=> o_F);
  
end structure;
