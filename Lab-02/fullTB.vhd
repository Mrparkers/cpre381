

library IEEE;
use IEEE.std_logic_1164.all;

-- This is an empty entity so we don't need to declare ports
entity fullTB is
 port (i : in std_logic); -- Random port so the thing actually works
 --generic(N : integer := 8);
end fullTB;

architecture behavior of fullTB is

component fullAdderNBitDataflow
  generic(N : integer := 8);
  port(i_A     : in std_logic_vector(N-1 downto 0);
       i_B     : in std_logic_vector(N-1 downto 0);
       i_Cin   : in std_logic;
       o_Cout  : out std_logic;
       o_sum   : out std_logic_vector(N-1 downto 0));
end component;

component fullAdderNBit
  generic(N : integer := 8);
  port(i_A     : in std_logic_vector(N-1 downto 0);
       i_B     : in std_logic_vector(N-1 downto 0);
       i_Cin   : in std_logic;
       o_Cout  : out std_logic;
       o_sum   : out std_logic_vector(N-1 downto 0));
end component;

constant N : integer := 8;
signal s_A : std_logic_vector(N-1 downto 0);
signal s_B : std_logic_vector(N-1 downto 0);
signal s_C : std_logic;
signal s_D : std_logic_vector(N-1 downto 0);
signal s_E : std_logic;
signal s_F : std_logic_vector(N-1 downto 0);
signal s_G : std_logic;

begin

DUT: fullAdderNBit
  port map(i_A  => s_A,
  	        i_B  => s_B,
		i_Cin  => s_C,
  	        o_sum  => s_D,
		o_Cout  => s_E);

  process
  begin

    s_A <= b"01010101";
    s_B <= b"00110011";
    s_C <= '1';
    wait for 100 ns;

  end process;

DUT2: fullAdderNBitDataflow
  port map(i_A  => s_A,
  	        i_B  => s_B,
		i_Cin  => s_C,
  	        o_sum  => s_F,
		o_Cout  => s_G);

  process
  begin

    s_A <= b"01010101";
    s_B <= b"00110011";
    s_C <= '1';
    wait for 100 ns;

  end process;
  
end behavior;
