


library IEEE;
use IEEE.std_logic_1164.all;

entity ones_complimenter_dataflow is
  generic(N : integer := 8);
  port(i_A  : in std_logic_vector(N-1 downto 0);
       o_F  : out std_logic_vector(N-1 downto 0));

end ones_complimenter_dataflow;

architecture dataflow of ones_complimenter_dataflow is

begin

o_F <= not i_A;

end dataflow;
