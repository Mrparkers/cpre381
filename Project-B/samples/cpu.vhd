-- cpu.vhd: Single-cycle implementation of MIPS32 for CprE 381, fall 2013,
-- Iowa State University
--
-- Zhao Zhang, fall 2013

-- The CPU entity. It connects to 1) an instruction memory, 2) a data memory, and 
-- 3) an external clock source.
--
-- Note: This is a partical sample
--

library IEEE;
use IEEE.std_logic_1164.all;
use work.mips32.all;

entity cpu is
  port (imem_addr   : out m32_word;     -- Instruction memory address
        inst        : in  m32_word;     -- Instruction
        dmem_addr   : out m32_word;     -- Data memory address
        dmem_read   : out m32_1bit;     -- Data memory read?
        dmem_write  : out m32_1bit;     -- Data memory write?
        dmem_wmask  : out m32_4bits;    -- Data memory write mask
        dmem_rdata  : in  m32_word;     -- Data memory read data
        dmem_wdata  : out m32_word;     -- Data memory write data
        reset       : in  m32_1bit;     -- Reset signal
        clock       : in  m32_1bit);    -- System clock
end cpu;

-- This architecture of CPU must be dominantly structural, with no bahavior 
-- modeling, and only data flow statements to copy/split/merge signals or 
-- with a single level of basic logic gates.
architecture structure of cpu is
  -- More code here

    component control is
        port(op_code     : in  m32_6bits;
             reg_dst     : out m32_1bit;
             alu_src     : out m32_1bit;
             mem_to_reg  : out m32_1bit;
             reg_write   : out m32_1bit;
             mem_read    : out m32_1bit;
             mem_write   : out m32_1bit;
             branch      : out m32_1bit;
             alu_op      : out m32_2bits;
             jump        : out m32_1bit);
    end component;

    -- The register file
    component regfile is
        port(src1       : in  m32_5bits;
             src2       : in  m32_5bits;
             dst        : in  m32_5bits;
             wdata      : in  m32_word;
             rdata1     : out m32_word;
             rdata2     : out m32_word;
             WE         : in  m32_1bit;
             reset      : in  m32_1bit;
             clock      : in  m32_1bit);
    end component;

    component ALU is 
        port(rdata1     : in  m32_word;
             rdata2     : in  m32_word;
             alu_code   : in  m32_4bits;
             result     : out m32_word;
             zero       : out m32_1bit);
    end component;

    component aluControl is
        port(aluOP      : in m32_2bits;
             funct      : in m32_6bits;
             output     : out m32_4bits);
    end component;

  -- 2-to-1 MUX
    component mux2to1 is
        generic (M      : integer := 1);    -- Number of bits in the inputs and output
        port (input0    : in m32_vector(M-1 downto 0);
              input1    : in m32_vector(M-1 downto 0);
              sel       : in m32_1bit;
              output    : out m32_vector(M-1 downto 0));
    end component;

    --
    -- Signals in the CPU
    --

    -- PC-related signals
    signal PC         : m32_word;     -- PC for the current inst
    -- More code here

    -- Instruction fields and derives
    signal opcode       : m32_6bits;    -- 6-bit opcode
    signal rs           : m32_5bits;
    signal rt           : m32_5bits;
    signal rd           : m32_5bits;
    signal ex_opcode    : m32_6bits;

    -- Control signals
    signal reg_dst      : m32_1bit;     -- Register destination
    signal alu_src      : m32_1bit;     -- Whether to use immediate or read
    signal mem_to_reg   : m32_1bit;     -- Whether to write memory to a register
    signal reg_write    : m32_1bit;     -- Write enable for register
    signal mem_read     : m32_1bit;     -- Whether to read from memory
    signal mem_write    : m32_1bit;     -- Whether to write to memory
    signal branch       : m32_1bit;     -- Whether to branch
    signal alu_op       : m32_2bits;    -- What the alu_op is
    signal jump         : m32_1bit;     -- Whether you should jump

    -- Other non-control signals connected to regfile
    signal wdata        : m32_word;     -- Register write data TODO
    signal alu_a        : m32_word;     -- Alu input 1
    signal alu_b        : m32_word;     -- Alu input 2
    signal alu_ctrl     : m32_word;     -- Control signal for the alu
    signal alu_res      : m32_word;
    signal alu_zero     : m32_1bit;

    signal clk          : m32_1bit := '0';
  -- More code here

begin
    -- 

    -- Jump target
    j_target <= PC(31 downto 28) & j_offset & "00";

    -- More code here
    
    -- Split the instructions into fields
    SPLIT : block
    begin
        opcode <= inst(31 downto 26);
        rs <= inst(25 downto 21);
        rt <= inst(20 downto 16);
        rd <= inst(15 downto 11);
        -- TODO Need to use mux as rd could be rd or rt depending on reg_dst from control
    end block;
  
  -- The mux connected to the dst port of regfile
    DST_MUX : mux2to1 generic map (M => 5)
        port map (rt, rd, reg_dst, dst);
  
    -- The control
    CONTROL1 : control
        port map(
            op_code     => opcode;
            reg_dst     => reg_dst;
            alu_src     => alu_src;
            mem_to_reg  => mem_to_reg;
            reg_write   => reg_write;
            mem_read    => mem_read;
            mem_write   => mem_write;
            branch      => branch;
            alu_op      => alu_op;
            jump        => jump);
        );

    -- The register file
    REGFILE1 : regfile
        port map(
            src1    => rs;
            src2    => rt;
            dst     => rd;
            wdata   => wdata;
            rdata1  => alu_a;
            rdata2  => alu_b;
            WE      => reg_write;
            reset   => '0';
            clk     => clk;
        );

    ALUCONTROL1 : aluControl
        port map(
            aluOP   => alu_op;
            funct   => ex_opcode;
            output  => alu_ctrl;
        );

    ALU_B_MUX: mux2to1

    ALU1: ALU
        port map(
            rdata1      => alu_a;
            rdata2      => alu_b;
            alu_code    => alu_ctrl;
            result      => alu_res;
            zero        => alu_zero;
        );

end structure;

