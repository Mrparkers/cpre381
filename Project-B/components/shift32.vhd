library IEEE;
use IEEE.std_logic_1164.all;
use work.mips32.all;

entity shift32 is
  port(input     : in m32_word;
       output    : out m32_word);

end shift32;

architecture dataflow of shift32 is

begin

output(31 downto 2) <= input(29 downto 0);
output(1 downto 0) <= (others => '0');

end dataflow;
