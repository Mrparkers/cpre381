
-- scpv1.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains a single cycle processor
--
--
-- NOTES:
-- 10/23/14 by QMM::Design created.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.mips32.all;

entity scpv1 is
  port (clock : in  m32_1bit); -- System clock
end scpv1;

architecture structural of scpv1 is
  -- Components
  
  component adder -- Add to Program Counter
    port (src1   : in  m32_word;
          src2   : in  m32_word;
          result : out m32_word);
  end component;
  
  component alu -- For Executing Instructions
    port (rdata1   : in  m32_word;
          rdata2   : in  m32_word;
          alu_code : in  m32_4bits;
          result   : out m32_word;
          zero     : out m32_1bit);
  end component;
  
  component mem -- Instruction Memory and Data Memory
  	 generic(depth_exp_of_2 	: integer := 32;
            mif_filename 	: string := "mem.mif");
   	port (address	: IN STD_LOGIC_VECTOR (depth_exp_of_2-1 DOWNTO 0) := (OTHERS => '0');
          byteena	: IN STD_LOGIC_VECTOR (3 DOWNTO 0) := (OTHERS => '1');
			    clock			: IN STD_LOGIC := '1';
		     	data	  	: IN STD_LOGIC_VECTOR (31 DOWNTO 0) := (OTHERS => '0');
		     	wren			 : IN STD_LOGIC := '0';
		     	q				   : OUT STD_LOGIC_VECTOR (31 DOWNTO 0));         
  end component;
  
  component reg -- Program Counter
    generic (M : integer := 32);                  -- Size of the register
    port (D     : in  m32_vector(M-1 downto 0);  -- Data input
          Q     : out m32_vector(M-1 downto 0);  -- Data output
          WE    : in  m32_1bit;                  -- Write enableenable
          reset : in  m32_1bit;                  -- The clock signal
          clock : in  m32_1bit);                 -- The reset signal
  end component;
  
  component regfile -- MIPS Register
    port (src1   : in  m32_5bits;
          src2   : in  m32_5bits;
          dst    : in  m32_5bits;
          wdata  : in  m32_word;
          rdata1 : out m32_word;
          rdata2 : out m32_word;
          WE     : in  m32_1bit;
          reset  : in  m32_1bit;
          clock  : in  m32_1bit);
  end component;
  
  component control -- Dataflow Control
    port (inst31to26 : in m32_6bits;
          regDst     : out m32_1bit;
          jump       : out m32_1bit;
          branch     : out m32_1bit;
          memRead    : out m32_1bit;
          memToReg   : out m32_1bit;
          aluOp      : out m32_2bits;
          memWrite   : out m32_1bit;
          aluSrc     : out m32_1bit;
          regWrite   : out m32_1bit);
  end component;
  
  component alucontrol -- ALU Control
    port (aluOp   : in m32_2bits;
          funct   : in m32_6bits;
          aluCode : out m32_4bits);
  end component;
  
  component mux2to1_5bit
    port(a : in m32_5bits;
         b : in m32_5bits;
         s : in m32_1bit;
         f : out m32_5bits);
  end component;
  
  component mux2to1_32bit
    port(a : in m32_word;
         b : in m32_word;
         s : in m32_1bit;
         f : out m32_word);
  end component;
  
  component leftshifttwo
    port(i : in m32_word;
         o : out m32_word);
  end component;

  component extsign16to32
    port(i : in std_logic_vector(15 downto 0);
         o : out std_logic_vector(31 downto 0));
  end component;
  
  -- Signals
  signal s_pc : m32_word := "00000000000000000000000000000000";
  signal s_pcPlusFour, s_inst, s_read1, s_read2, s_write, s_signext16, s_aluB, s_aluResult, s_memOut, s_jumpExt, s_preJump, s_jumpAddress, s_branchAluB, s_branchAddress, s_branchMuxOut, s_jumpMuxOut : m32_word;
  signal s_regDst, s_jump, s_branch, s_memRead, s_memToReg, s_memWrite, s_aluSrc, s_regWrite, s_zero, s_branchAndZero : m32_1bit;
  signal s_aluOp : m32_2bits;
  signal s_aluCode : m32_4bits;
  signal s_writeLocation : m32_5bits;
  
  begin
    PC: reg
      port map (D => s_jumpMuxOut,
                Q => s_pc,
                WE => '1',
                reset => '0',
                clock => clock);
               
    PCPLUSFOUR: adder
      port map (src1 => s_pc,
                src2 => "00000000000000000000000000000100",
                result => s_pcPlusFour);
               
    INST: mem
      generic map(mif_filename => "imem.mif")
      port map (address => s_pc,
                byteena => "1111",
                clock => clock,
                wren => '0',
                q => s_inst);
    --s_inst <= "00000000100011001000110010100000";
    
    CTRL: control
      port map (inst31to26 => s_inst(31 downto 26),
                regDst => s_regDst,
                jump => s_jump,
                branch => s_branch,
                memRead => s_memRead,
                memToReg => s_memToReg,
                aluOp => s_aluOp,
                memWrite => s_memWrite,
                aluSrc => s_aluSrc,
                regWrite => s_regWrite);
                
    ALUCTRL: alucontrol
      port map (aluOp => s_aluOp,
                funct => s_inst(5 downto 0),
                aluCode => s_aluCode);
    
    WRITELOCATION: mux2to1_5bit
      port map (a => s_inst(20 downto 16),
                b => s_inst(15 downto 11),
                s => s_regDst,
                f => s_writeLocation);
                
    REGISTERS: regfile
      port map (src1 => s_inst(25 downto 21),
                src2 => s_inst(20 downto 16),
                dst => s_writeLocation,
                wdata => s_write,
                rdata1 => s_read1,
                rdata2 => s_read2,
                WE => s_regWrite,
                reset => '0',
                clock => clock);
    
    -- ALU AND MEMORY STUFF STARTS HERE
    
    SIGNEXT: extsign16to32
      port map (i => s_inst(15 downto 0),
                o => s_signext16);
    
    ALUSRC: mux2to1_32bit
      port map(a => s_read2,
               b => s_signext16,
               s => s_aluSrc,
               f => s_aluB);
               
    CALCULATIONS: alu
      port map(rdata1 => s_read1,
               rdata2 => s_aluB,
               alu_code => s_aluCode,
               result => s_aluResult,
               zero => s_zero);
    
    DMEM: mem
      generic map(mif_filename => "dmem.mif")
      port map (address => s_aluResult,
                byteena => "1111",
                data => s_read2,
                clock => clock,
                wren => s_memRead,
                q => s_memOut);

    MEMOUT: mux2to1_32bit
      port map(a => s_memOut,
               b => s_aluResult,
               s => s_memToReg,
               f => s_write);
    
    -- JUMP AND BRANCH STUFF STARTS HERE
    
    JUMPEXT:
      s_jumpExt(31) <= '0';
      s_jumpExt(30) <= '0';
      s_jumpExt(29) <= '0';
      s_jumpExt(28) <= '0';
      s_jumpExt(27) <= '0';
      s_jumpExt(26) <= '0';
      G0: for i in 0 to 25 generate
        s_jumpExt(i) <= s_inst(i);
      end generate;
    
    
    PCLEFTTWO: leftshifttwo
      port map(i => s_jumpExt,
               o => s_preJump);
               
    FIXJUMPADDRESS:
      s_jumpAddress(31) <= s_pcPlusFour(31);
      s_jumpAddress(30) <= s_pcPlusFour(30);
      s_jumpAddress(29) <= s_pcPlusFour(29);
      s_jumpAddress(28) <= s_pcPlusFour(28);
    
    BRANCHLEFTTWO: leftshifttwo
      port map(i => s_signext16,
               o => s_branchAluB);
               
    BRANCHALU: alu
      port map(rdata1 => s_pcPlusFour,
               rdata2 => s_branchAluB,
               alu_code => "0010",
               result => s_branchAddress);
               
    s_branchAndZero <= s_branch and s_zero;
               
    BRANCHMUX: mux2to1_32bit
      port map(a => s_pcPlusFour,
               b => s_branchAddress,
               s => s_branchAndZero,
               f => s_branchMuxOut);
               
    JUMPMUX: mux2to1_32bit
      port map(a => s_branchMuxOut,
               b => s_jumpAddress,
               s => s_jump,
               f => s_jumpMuxOut);
               
end structural;
