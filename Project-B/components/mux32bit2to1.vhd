library IEEE;
use IEEE.std_logic_1164.all;
use work.mips32.all;

entity mux32bit2to1 is
  port(one     : in m32_word; --if sel is 0
       two     : in m32_word; --if sel is 1
       sel     : in m32_1bit;
       output  : out m32_word);

end mux32bit2to1;

architecture dataflow of mux32bit2to1 is

begin
    with sel select output <= one when '0', two when others; 

end dataflow;
