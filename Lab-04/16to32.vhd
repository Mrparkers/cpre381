
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity sixteenTo32 is
  port(i_16     : in std_logic_vector(15 downto 0);
       sign    : in std_logic;
       o_32   : out std_logic_vector(31 downto 0));

end sixteenTo32;

architecture dataflow of sixteenTo32 is

begin
process (i_16, sign)
begin
if (sign = '0') then
	--o_32 <= std_logic_vector(resize(unsigned(i_8), o_32'width));
	o_32(15 downto 0) <= i_16;
	o_32(31 downto 16) <= (others => '0');
else
	o_32(15 downto 0) <= i_16;
	o_32(31 downto 16) <= (others => i_16(15));
end if;
end process;  

end dataflow;
