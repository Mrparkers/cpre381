

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.util.all;

entity memTB is
   generic(gCLK_HPER   : time := 50 ns);
end memTB;

architecture behavior of memTB is

constant cCLK_PER  : time := gCLK_HPER * 2;

component mem
	generic(depth_exp_of_2 	: integer := 10;
			mif_filename 	: string := "mem.mif");
	port   (address			: IN STD_LOGIC_VECTOR (depth_exp_of_2-1 DOWNTO 0) := (OTHERS => '0');
			byteena			: IN STD_LOGIC_VECTOR (3 DOWNTO 0) := (OTHERS => '1');
			clock			: IN STD_LOGIC := '1';
			data			: IN STD_LOGIC_VECTOR (31 DOWNTO 0) := (OTHERS => '0');
			wren			: IN STD_LOGIC := '0';
			q				: OUT STD_LOGIC_VECTOR (31 DOWNTO 0));       
end component;

constant N : integer := 8;
signal s_address : STD_LOGIC_VECTOR (10-1 DOWNTO 0);
signal s_byteena : STD_LOGIC_VECTOR (3 DOWNTO 0);
signal s_clock : STD_LOGIC;
signal s_data : STD_LOGIC_VECTOR (31 DOWNTO 0);
signal s_wren : STD_LOGIC;
signal s_q : STD_LOGIC_VECTOR (31 DOWNTO 0);
signal s_storage : array32_bit(9 downto 0); --used for temp storage of the read values

begin

DUT: mem
  port map(address  => s_address,
  	        byteena  => s_byteena,
		clock  => s_clock,
  	        data  => s_data,
		wren  => s_wren,
		q  => s_q);

  P_CLK: process
  begin
    s_clock <= '0';
    wait for gCLK_HPER;
    s_clock <= '1';
    wait for gCLK_HPER;
  end process;

  P_init: process
  begin

	for i in 0 to 9 loop
		s_address <= std_logic_vector(to_unsigned(i, 10));
		s_byteena <= "0000";
		s_data <= x"00000000";
		s_wren <= '0';
		wait for cCLK_PER;
		s_storage(i) <= s_q;
	end loop;

	wait for cCLK_PER;

	for i in 100 to 109 loop
		s_address <= std_logic_vector(to_unsigned(i, 10));
		s_byteena <= "1111";
		s_data <= s_storage(i - 100);
		s_wren <= '1';
		wait for cCLK_PER;
	end loop;

	wait for cCLK_PER;

	for i in 100 to 109 loop
		s_address <= std_logic_vector(to_unsigned(i, 10));
		s_byteena <= "0000";
		s_data <= x"00000000";
		s_wren <= '0';
		wait for cCLK_PER;
		s_storage(i - 100) <= s_q;
	end loop;

  end process;
  
end behavior;












