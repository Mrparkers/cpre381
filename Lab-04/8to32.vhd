
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity eightTo32 is
  port(i_8     : in std_logic_vector(7 downto 0);
       sign    : in std_logic;
       o_32   : out std_logic_vector(31 downto 0));

end eightTo32;

architecture dataflow of eightTo32 is

begin
process (i_8, sign)
begin
if (sign = '0') then
	--o_32 <= std_logic_vector(resize(unsigned(i_8), o_32'width));
	o_32(7 downto 0) <= i_8;
	o_32(31 downto 8) <= (others => '0');
else
	o_32(7 downto 0) <= i_8;
	o_32(31 downto 8) <= (others => i_8(7));
end if;
end process;  

end dataflow;
