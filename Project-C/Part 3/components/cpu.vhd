

-- cpu.vhd: Single-cycle implementation of MIPS32 for CprE 381, fall 2013,
-- Iowa State University
--
-- Zhao Zhang, fall 2013
 
-- The CPU entity. It connects to 1) an instruction memory, 2) a data memory, and
-- 3) an external clock source.
--
-- Note: This is a partical sample
--
 
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_misc.all;
use work.mips32.all;
 
entity cpu is
  port (imem_addr   : out m32_word;     -- Instruction memory address
        inst        : in  m32_word;     -- Instruction
        dmem_addr   : out m32_word;     -- Data memory address
        dmem_read   : out m32_1bit;     -- Data memory read?
        dmem_write  : out m32_1bit;     -- Data memory write?
        dmem_wmask  : out m32_4bits;    -- Data memory write mask
        dmem_rdata  : in  m32_word;     -- Data memory read data
        dmem_wdata  : out m32_word;     -- Data memory write data
        reset       : in  m32_1bit;     -- Reset signal
        clock       : in  m32_1bit);    -- System clock
end cpu;
 
-- This architecture of CPU must be dominantly structural, with no bahavior
-- modeling, and only data flow statements to copy/split/merge signals or
-- with a single level of basic logic gates.
architecture structure of cpu is
  -- More code here
 
    component control is
        port(op_code     : in  m32_6bits;
             reg_dst     : out m32_1bit;
             alu_src     : out m32_1bit;
             mem_to_reg  : out m32_1bit;
             reg_write   : out m32_1bit;
             mem_read    : out m32_1bit;
             mem_write   : out m32_1bit;
             branch      : out m32_2bits;
             alu_op      : out m32_2bits;
             jump        : out m32_1bit);
    end component;
 
    -- The register file
    component regfile is
        port(src1       : in  m32_5bits;
             src2       : in  m32_5bits;
             dst        : in  m32_5bits;
             wdata      : in  m32_word;
             rdata1     : out m32_word;
             rdata2     : out m32_word;
             WE         : in  m32_1bit;
             reset      : in  m32_1bit;
             clock      : in  m32_1bit);
    end component;

    component reg is
        generic (M : integer := 32);                  -- Size of the register
        port(D      : in  m32_vector(M-1 downto 0);  -- Data input
             Q      : out m32_vector(M-1 downto 0);  -- Data output
             WE     : in  m32_1bit;                  -- Write enableenable
             reset  : in  m32_1bit;                  -- The clock signal
             clock  : in  m32_1bit);                 -- The reset signal
    end component;
 
    component ALU is
        port(rdata1     : in  m32_word;
             rdata2     : in  m32_word;
             alu_code   : in  m32_4bits;
             result     : out m32_word;
             zero       : out m32_1bit);
    end component;
 
    component aluControl is
        port(aluOP      : in m32_2bits;
             funct      : in m32_6bits;
             output     : out m32_4bits);
    end component;

    component adder is
        port (src1    : in  m32_word;
              src2    : in  m32_word;
              result  : out m32_word);
    end component;
 
    component mux32bit2to1 is
        port(one        : in m32_word; --if sel is 0
             two        : in m32_word; --if sel is 1
             sel        : in m32_1bit;
             output     : out m32_word);
    end component;

    component mux5bit2to1 is
        port(one        : in m32_5bits;
             two        : in m32_5bits;
             sel        : in m32_1bit;
             output     : out m32_5bits);
    end component;
   
    component extend16to32 is
        port(input      : in m32_halfword;
             sign       : in m32_1bit;
             output     : out m32_word);
    end component;

    component extend5to32 is
      port(input     : in m32_5bits;
           sign      : in m32_1bit;
           output    : out m32_word);
    end component;

    component shift32 is
        port(input     : in m32_word;
             output    : out m32_word);
    end component;

    -- PC-related signals
    signal PC           : m32_word := "00000000000000000000000000000000";     -- PC for the current inst
    signal pc_plus_4    : m32_word;
    signal pc_plus_imm  : m32_word;
    signal pc_branch    : m32_word;
    signal pc_final     : m32_word;

    signal take_branch  : m32_1bit;
    signal j_target     : m32_word;     -- jump target
 
    -- Instruction fields and derives
    signal opcode       : m32_6bits;    -- 6-bit opcode
    signal rs           : m32_5bits;
    signal rt           : m32_5bits;
    signal rd           : m32_5bits;
    signal shift        : m32_5bits;
    signal shift_ext    : m32_word;
    signal ex_opcode    : m32_6bits;
    signal immediate    : m32_halfword;
    signal imm_ext      : m32_word;
    signal imm_ext_sht  : m32_word;
    signal j_imm        : m32_26bits;
 
    -- Control signals
    signal reg_dst      : m32_1bit;     -- Register destination
    signal alu_src      : m32_1bit;     -- Whether to use immediate or read
    signal mem_to_reg   : m32_1bit;     -- Whether to write memory to a register
    signal reg_write    : m32_1bit;     -- Write enable for register
    signal mem_read     : m32_1bit;     -- Whether to read from memory
    signal mem_write    : m32_1bit;     -- Whether to write to memory
    signal branch       : m32_2bits;     -- Whether to branch
    signal alu_op       : m32_2bits;    -- What the alu_op is
    signal jump         : m32_1bit;     -- Whether you should jump

    signal is_not_shift     : m32_1bit;
 
    -- Other non-control signals connected to regfile
    signal wdata        : m32_word;     -- Register write data
    signal dst_reg      : m32_5bits;    -- regfile dest register

    signal alu_a        : m32_word;     -- Alu input 1
    signal alu_a_inter  : m32_word;
    signal alu_b_inter  : m32_word;     -- Alu input 2 intermediate, before muxed
    signal alu_b        : m32_word;     -- Alu input 2
    signal alu_ctrl     : m32_4bits;     -- Control signal for the alu
    signal alu_res      : m32_word;
    signal alu_zero     : m32_1bit;
 
begin
    imem_addr <= PC;
    dmem_addr <= alu_res;
    dmem_read <= mem_read;
    dmem_write <= mem_write;
    dmem_wmask <= x"F";
    dmem_wdata <= alu_b_inter;
 
    -- Jump target
    j_target <= pc_plus_4(31 downto 28) & j_imm & "00";
   
    -- Split the instructions into fields
    SPLIT : block
    begin
        opcode <= inst(31 downto 26);
        rs <= inst(25 downto 21);
        rt <= inst(20 downto 16);
        rd <= inst(15 downto 11);
        shift <= inst(10 downto 6);
        ex_opcode <= inst(5 downto 0);
        immediate <= inst(15 downto 0);
        j_imm <= inst(25 downto 0);
    end block;

    PC_REG: reg
        port map(
            D       => pc_final,
            Q       => PC,
            WE      => '1',
            reset   => reset,
            clock   => clock
        );

    WDATA_MUX: mux32bit2to1
        port map(
            one     => alu_res,
            two     => dmem_rdata,
            sel     => mem_to_reg,
            output  => wdata
        );

    DST_REG_MUX: mux5bit2to1
        port map(
            one     => rt,
            two     => rd,
            sel     => reg_dst,
            output  => dst_reg
        );
 
    IMM_SIGN_EXTEND: extend16to32
        port map(
            input   => immediate,
            sign    => '1',
            output  => imm_ext
        );

    SHIFT_ZERO_EXTEND: extend5to32
        port map(
            input   => shift,
            sign    => '0',
            output  => shift_ext
        );

    IMM_SHIFT: shift32
        port map(
            input   => imm_ext,
            output  => imm_ext_sht
        );
 
    -- The control
    CONTROL1: control
        port map(
            op_code     => opcode,
            reg_dst     => reg_dst,
            alu_src     => alu_src,
            mem_to_reg  => mem_to_reg,
            reg_write   => reg_write,
            mem_read    => mem_read,
            mem_write   => mem_write,
            branch      => branch,
            alu_op      => alu_op,
            jump        => jump
        );
 
    -- The register file
    REGFILE1: regfile
        port map(
            src1    => rs,
            src2    => rt,
            dst     => dst_reg,
            wdata   => wdata,
            rdata1  => alu_a_inter,
            rdata2  => alu_b_inter,
            WE      => reg_write,
            reset   => reset,
            clock   => clock
        );
 
    ALUCONTROL1: aluControl
        port map(
            aluOP   => alu_op,
            funct   => ex_opcode,
            output  => alu_ctrl
        );
 
    ALU_B_MUX: mux32bit2to1
        port map(
            one     => alu_b_inter,
            two     => imm_ext,
            sel     => alu_src,
            output  => alu_b
        );

    is_not_shift <= or_reduce(opcode or ex_opcode);

    ALU_B_MUX_2: mux32bit2to1
        port map(
            one     => shift_ext,
            two     => alu_a_inter,
            sel     => is_not_shift,
            output  => alu_a
        );
 
    ALU1: ALU
        port map(
            rdata1      => alu_a,
            rdata2      => alu_b,
            alu_code    => alu_ctrl,
            result      => alu_res,
            zero        => alu_zero
        );

    take_branch <= ((branch(1) and not branch(0) and alu_zero) or (branch(1) and branch(0) and not alu_zero));
    
    PC_INC_ADD: adder
        port map(
            src1        => PC,
            src2        => "00000000000000000000000000000100",
            result      => pc_plus_4
        );

    PC_BRANCH_ADD: adder
        port map(
            src1        => pc_plus_4,
            src2        => imm_ext_sht,
            result      => pc_plus_imm
        );

    PC_BRANCH_MUX: mux32bit2to1
        port map(
            one         => pc_plus_4,
            two         => pc_plus_imm,
            sel         => take_branch,
            output      => pc_branch
        );

    PC_JUMP_MUX: mux32bit2to1
        port map(
            one         => pc_branch,
            two         => j_target,
            sel         => jump,
            output      => pc_final
        );
 
end structure;


