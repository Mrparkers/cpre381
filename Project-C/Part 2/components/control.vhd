-- control.vhd: CprE 381 F13 template file
-- 
-- The main control unit of MIPS

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.mips32.all;

entity control is
  port (op_code     : in  m32_6bits;
        reg_dst     : out m32_1bit;
        alu_src     : out m32_1bit;
        mem_to_reg  : out m32_1bit;
        reg_write   : out m32_1bit;
        mem_read    : out m32_1bit;
        mem_write   : out m32_1bit;
        branch      : out m32_2bits;
        alu_op      : out m32_2bits;
        jump        : out m32_1bit;
        write       : in m32_1bit);
end control;

architecture rom of control is
  subtype code_t is m32_vector(10 downto 0);
  type rom_t is array (0 to 63) of code_t;
  
  -- The ROM content
  -- Format: reg_dst, alu_src, mem_to_reg, reg_write, mem_read, 
  -- mem_write, branch, alu_op(1), alu_op(0), jump
  signal rom : rom_t := (
    -- More code here
    0  => "10010000100",    -- r type
    2  => "---00000--1",    -- j
    4  => "-0-00010010",    -- beq
    5  => "-0-00011010",    -- bne
    8  => "01010000000",    -- addi
    35 => "01111000000",    -- lw
    43 => "-1-00100000",    -- sw
    others=>"00000000000");

begin

    (reg_dst, alu_src, mem_to_reg, reg_write, mem_read, mem_write, branch(1), branch(0), alu_op(1), alu_op(0), jump)
        <= rom(to_integer(unsigned(op_code))) when write = '1'
        else rom(63);    -- doesn't exist so returns 0's

end rom;

