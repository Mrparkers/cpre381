

-- cpu.vhd: Single-cycle implementation of MIPS32 for CprE 381, fall 2013,
-- Iowa State University
--
-- Zhao Zhang, fall 2013
 
-- The CPU entity. It connects to 1) an instruction memory, 2) a data memory, and
-- 3) an external clock source.
--
-- Note: This is a partical sample
--
 
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_misc.all;
use work.mips32.all;
 
entity ID_EX is
  port (i_reg_write     : in m32_1bit;
        o_reg_write     : out m32_1bit;
        i_mem_to_reg    : in m32_1bit;
        o_mem_to_reg    : out m32_1bit;
        i_branch        : in m32_2bits;
        o_branch        : out m32_2bits;
        i_mem_read      : in m32_1bit;
        o_mem_read      : out m32_1bit;
        i_mem_write     : in m32_1bit;
        o_mem_write     : out m32_1bit;
        i_alu_src       : in m32_1bit;
        o_alu_src       : out m32_1bit;
        i_alu_op        : in m32_2bits;
        o_alu_op        : out m32_2bits;
        i_reg_dst       : in m32_1bit;
        o_reg_dst       : out m32_1bit;

        -- Other signals --
        i_opcode        : in m32_6bits;
        o_opcode        : out m32_6bits;
        i_pc            : in m32_word;
        o_pc            : out m32_word;
        i_rdata1        : in m32_word;
        o_rdata1        : out m32_word;
        i_rdata2        : in m32_word;
        o_rdata2        : out m32_word;
        i_imm_ext       : in m32_word;
        o_imm_ext       : out m32_word;
        i_rs            : in m32_5bits;
        o_rs            : out m32_5bits;
        i_rt            : in m32_5bits;
        o_rt            : out m32_5bits;
        i_rd            : in m32_5bits;
        o_rd            : out m32_5bits;
		i_jump			: in m32_1bit;
		o_jump			: out m32_1bit;
		i_j_imm       : in m32_26bits;
        o_j_imm       : out m32_26bits;
        clock           : in m32_1bit;
        reset         : in  m32_1bit);    -- System clock
end ID_EX;
 
-- This architecture of CPU must be dominantly structural, with no bahavior
-- modeling, and only data flow statements to copy/split/merge signals or
-- with a single level of basic logic gates.
architecture structure of ID_EX is

    component reg is
        generic (M : integer := 32);                  -- Size of the register
        port(D      : in  m32_vector(M-1 downto 0);  -- Data input
             Q      : out m32_vector(M-1 downto 0);  -- Data output
             WE     : in  m32_1bit;                  -- Write enableenable
             reset  : in  m32_1bit;                  -- The clock signal
             clock  : in  m32_1bit);                 -- The reset signal
    end component;

    component reg_1bit is
        port(D      : in  m32_1bit;  -- Data input
             Q      : out m32_1bit;  -- Data output
             WE     : in  m32_1bit;                  -- Write enableenable
             reset  : in  m32_1bit;                  -- The clock signal
             clock  : in  m32_1bit);                 -- The reset signal
    end component;
 
begin

    reg_write_reg: reg_1bit
        port map(
            D		=> i_reg_write,
            Q		=> o_reg_write,
            WE		=> '1',
            reset	=> reset,
            clock	=> clock
        );
    mem_to_reg_reg: reg_1bit
        port map(
            D		=> i_mem_to_reg,
            Q		=> o_mem_to_reg,
            WE		=> '1',
            reset	=> reset,
            clock	=> clock
        );
    branch_reg: reg generic map(2)
        port map(
            D		=> i_branch,
            Q		=> o_branch,
            WE		=> '1',
            reset	=> reset,
            clock	=> clock
        );
    mem_read_reg: reg_1bit
        port map(
            D		=> i_mem_read,
            Q		=> o_mem_read,
            WE		=> '1',
            reset	=> reset,
            clock	=> clock
        );
    mem_write_reg: reg_1bit
        port map(
            D		=> i_mem_write,
            Q		=> o_mem_write,
            WE		=> '1',
            reset	=> reset,
            clock	=> clock
        );
    alu_src_reg: reg_1bit
        port map(
            D		=> i_alu_src,
            Q		=> o_alu_src,
            WE		=> '1',
            reset	=> reset,
            clock	=> clock
        );
    alu_op_reg: reg
        generic map(2)
        port map(
            D		=> i_alu_op,
            Q		=> o_alu_op,
            WE		=> '1',
            reset	=> reset,
            clock	=> clock
        );
    reg_dst_reg: reg_1bit
        port map(
            D		=> i_reg_dst,
            Q		=> o_reg_dst,
            WE		=> '1',
            reset	=> reset,
            clock	=> clock
        );
    pc_reg: reg
        generic map(32)
        port map(
            D		=> i_pc,
            Q		=> o_pc,
            WE		=> '1',
            reset	=> reset,
            clock	=> clock
        );
    rdata1_reg: reg
        generic map(32)
        port map(
            D		=> i_rdata1,
            Q		=> o_rdata1,
            WE		=> '1',
            reset	=> reset,
            clock	=> clock
        );
    rdata2_reg: reg
        generic map(32)
        port map(
            D		=> i_rdata2,
            Q		=> o_rdata2,
            WE		=> '1',
            reset	=> reset,
            clock	=> clock
        );
    imm_ext_sht_reg: reg
        generic map(32)
        port map(
            D		=> i_imm_ext,
            Q		=> o_imm_ext,
            WE		=> '1',
            reset	=> reset,
            clock	=> clock
        );

    rs_reg: reg
        generic map(5)
        port map(
            D       => i_rs,
            Q       => o_rs,
            WE      => '1',
            reset   => reset,
            clock   => clock
        );

    rt_reg: reg
        generic map(5)
        port map(
            D		=> i_rt,
            Q		=> o_rt,
            WE		=> '1',
            reset	=> reset,
            clock	=> clock
        );
    rd_reg: reg
        generic map(5)
        port map(
            D		=> i_rd,
            Q		=> o_rd,
            WE		=> '1',
            reset	=> reset,
            clock	=> clock
        );
    opcode_reg: reg
        generic map(6)
        port map(
            D		=> i_opcode,
            Q		=> o_opcode,
            WE		=> '1',
            reset	=> reset,
            clock	=> clock
        );
    jump_reg: reg_1bit
        port map(
            D		=> i_jump,
            Q		=> o_jump,
            WE		=> '1',
            reset	=> reset,
            clock	=> clock
        );		

    j_imm_reg: reg generic map(26)
        port map(
            D       => i_j_imm,
            Q       => o_j_imm,
            WE      => '1',
            reset   => reset,
            clock   => clock
        );

end structure;


