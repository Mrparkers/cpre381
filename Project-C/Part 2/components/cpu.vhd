-- cpu.vhd: Single-cycle implementation of MIPS32 for CprE 381, fall 2013,
-- Iowa State University
--
-- Zhao Zhang, fall 2013
 
-- The CPU entity. It connects to 1) an instruction memory, 2) a data memory, and
-- 3) an external clock source.
--
-- Note: This is a partical sample
--
 
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_misc.all;
use work.mips32.all;
 
entity cpu is
  port (imem_addr   : out m32_word;     -- Instruction memory address
        inst        : in  m32_word;     -- Instruction
        dmem_addr   : out m32_word;     -- Data memory address
        dmem_read   : out m32_1bit;     -- Data memory read?
        dmem_write  : out m32_1bit;     -- Data memory write?
        dmem_wmask  : out m32_4bits;    -- Data memory write mask
        dmem_rdata  : in  m32_word;     -- Data memory read data
        dmem_wdata  : out m32_word;     -- Data memory write data
        reset       : in  m32_1bit;     -- Reset signal
        clock       : in  m32_1bit);    -- System clock
end cpu;
 
-- This architecture of CPU must be dominantly structural, with no bahavior
-- modeling, and only data flow statements to copy/split/merge signals or
-- with a single level of basic logic gates.
architecture structure of cpu is
  -- More code here
 
    component control is
        port(op_code     : in  m32_6bits;
             reg_dst     : out m32_1bit;
             alu_src     : out m32_1bit;
             mem_to_reg  : out m32_1bit;
             reg_write   : out m32_1bit;
             mem_read    : out m32_1bit;
             mem_write   : out m32_1bit;
             branch      : out m32_2bits;
             alu_op      : out m32_2bits;
             jump        : out m32_1bit;
             write       : in m32_1bit);
    end component;
 
    -- The register file
    component regfile is
        port(src1       : in  m32_5bits;
             src2       : in  m32_5bits;
             dst        : in  m32_5bits;
             wdata      : in  m32_word;
             rdata1     : out m32_word;
             rdata2     : out m32_word;
             WE         : in  m32_1bit;
             reset      : in  m32_1bit;
             clock      : in  m32_1bit);
    end component;

    component reg is
        generic (M : integer := 32);                  -- Size of the register
        port(D      : in  m32_vector(M-1 downto 0);  -- Data input
             Q      : out m32_vector(M-1 downto 0);  -- Data output
             WE     : in  m32_1bit;                  -- Write enableenable
             reset  : in  m32_1bit;                  -- The clock signal
             clock  : in  m32_1bit);                 -- The reset signal
    end component;
 
    component ALU is
        port(rdata1     : in  m32_word;
             rdata2     : in  m32_word;
             alu_code   : in  m32_4bits;
             result     : out m32_word;
             zero       : out m32_1bit);
    end component;
 
    component aluControl is
        port(aluOP      : in m32_2bits;
             funct      : in m32_6bits;
             output     : out m32_4bits);
    end component;

    component adder is
        port (src1    : in  m32_word;
              src2    : in  m32_word;
              result  : out m32_word);
    end component;
 
    component mux32bit2to1 is
        port(one        : in m32_word; --if sel is 0
             two        : in m32_word; --if sel is 1
             sel        : in m32_1bit;
             output     : out m32_word);
    end component;

    component mux5bit2to1 is
        port(one        : in m32_5bits;
             two        : in m32_5bits;
             sel        : in m32_1bit;
             output     : out m32_5bits);
    end component;
   
    component extend16to32 is
        port(input      : in m32_halfword;
             sign       : in m32_1bit;
             output     : out m32_word);
    end component;

    component extend5to32 is
      port(input     : in m32_5bits;
           sign      : in m32_1bit;
           output    : out m32_word);
    end component;

    component shift32 is
        port(input     : in m32_word;
             output    : out m32_word);
    end component;

    component mux32bit4to1 is
        port(zero     : in m32_word;
             one      : in m32_word;
             two      : in m32_word;
             three    : in m32_word;
             sel      : in m32_2bits;
             output   : out m32_word);
    end component;

    component forwarding_unit is
        port(id_ex_rs           : in m32_5bits;
             id_ex_rt           : in m32_5bits;
             ex_mem_rd          : in m32_5bits;
             mem_wb_rd          : in m32_5bits;
             ex_mem_reg_write   : in m32_1bit;
             mem_wb_reg_write   : in m32_1bit;
             forward_a          : out m32_2bits;
             forward_b          : out m32_2bits);
    end component;

    component hazard_detection is
        port(id_ex_mem_read     : in m32_1bit;
             if_id_rs           : in m32_5bits;
             if_id_rt           : in m32_5bits;
             id_ex_rt           : in m32_5bits;
             pc_write           : out m32_1bit;
             if_id_write        : out m32_1bit;
             control_write      : out m32_1bit);
    end component;

    component IF_ID is
        port(i_inst      : in m32_word;
             o_inst      : out m32_word;
             i_pc        : in m32_word;
             o_pc        : out m32_word;
             clock       : in m32_1bit;
			 reset       : in  m32_1bit;
             we          : in m32_1bit);    -- System clock
    end component;

    component ID_EX is
          port(
			i_reg_write     : in m32_1bit;
			o_reg_write     : out m32_1bit;
			i_mem_to_reg    : in m32_1bit;
			o_mem_to_reg    : out m32_1bit;
			i_branch        : in m32_2bits;
			o_branch        : out m32_2bits;
			i_mem_read      : in m32_1bit;
			o_mem_read      : out m32_1bit;
			i_mem_write     : in m32_1bit;
			o_mem_write     : out m32_1bit;
			i_alu_src       : in m32_1bit;
			o_alu_src       : out m32_1bit;
			i_alu_op        : in m32_2bits;
			o_alu_op        : out m32_2bits;
			i_reg_dst       : in m32_1bit;
			o_reg_dst       : out m32_1bit;

			i_opcode        : in m32_6bits;
			o_opcode        : out m32_6bits;
			i_pc            : in m32_word;
			o_pc            : out m32_word;
			i_rdata1        : in m32_word;
			o_rdata1        : out m32_word;
			i_rdata2        : in m32_word;
			o_rdata2        : out m32_word;
			i_imm_ext       : in m32_word;
			o_imm_ext       : out m32_word;
            i_rs            : in m32_5bits;
            o_rs            : out m32_5bits;
			i_rt            : in m32_5bits;
			o_rt            : out m32_5bits;
			i_rd            : in m32_5bits;
			o_rd            : out m32_5bits;
			i_jump			: in m32_1bit;
			o_jump			: out m32_1bit;
		    i_j_imm       : in m32_26bits;
            o_j_imm       : out m32_26bits;
			clock           : in m32_1bit;
			reset         : in  m32_1bit);    -- System clock  
    end component;
	
	component EX_MEM is
		port(
			-- Writeback signals from control (2) --
			i_mem_to_reg  : in  m32_1bit;     -- Input  WB #1
			o_mem_to_reg  : out m32_1bit;     -- Output WB #1
			i_reg_write   : in  m32_1bit;     -- Input  WB #2
			o_reg_write   : out m32_1bit;     -- Output WB #2
			-- MEM signals from control (3) --
			i_mem_write   : in  m32_1bit;     -- Input  MEM #1
			o_mem_write   : out m32_1bit;     -- Output MEM #1
			i_mem_read    : in  m32_1bit;     -- Input  MEM #2
			o_mem_read    : out m32_1bit;     -- Output MEM #2
			i_branch      : in  m32_2bits;    -- Input  MEM #3
			o_branch      : out m32_2bits;    -- Output MEM #3
			-- Other MEM signals (5) --
			i_pc_plus_imm : in  m32_word;     -- Input  Other #1
			o_pc_plus_imm : out m32_word;     -- Output Other #1
			i_alu_zero    : in  m32_1bit;
			o_alu_zero    : out m32_1bit;
			i_alu_res     : in  m32_word;
			o_alu_res     : out m32_word;
			i_rdata2      : in  m32_word;
			o_rdata2      : out m32_word;
			i_dst_reg     : in  m32_5bits;
			o_dst_reg     : out m32_5bits;
			i_pc		  : in  m32_word;
			o_pc		  : out m32_word;
			i_jump		  : in m32_1bit;
			o_jump		  : out m32_1bit;
		    i_j_imm       : in m32_26bits;
            o_j_imm       : out m32_26bits;
			-- Clock --
			clock         : in  m32_1bit;
			reset         : in  m32_1bit);    -- System clock
	end component;
	
	component MEM_WB is
		port(
			-- Writeback signals from control (2) --
			i_mem_to_reg  : in  m32_1bit;
			o_mem_to_reg  : out m32_1bit;
			i_reg_write   : in  m32_1bit;
			o_reg_write   : out m32_1bit;
			-- Other MEM signals (3) --
			i_alu_res     : in  m32_word;
			o_alu_res     : out m32_word;
			i_rdata       : in  m32_word;
			o_rdata       : out m32_word;
			i_dst_reg     : in  m32_5bits;
			o_dst_reg     : out m32_5bits;
			-- Clock --
			clock         : in  m32_1bit;
			reset         : in  m32_1bit
		);
	end component;

    -- PC-related signals
    signal PC           : m32_word := "00000000000000000000000000000000";     -- PC for the current inst
    signal pc_plus_4    : m32_word;
    signal pc_plus_imm  : m32_word;
    signal pc_branch    : m32_word;
    signal pc_final     : m32_word;

    signal take_branch  : m32_1bit;
    signal j_target     : m32_word;     -- jump target
 
    -- Instruction fields and derives
    signal opcode       : m32_6bits;    -- 6-bit opcode
    signal rs           : m32_5bits;
    signal rt           : m32_5bits;
    signal rd           : m32_5bits;
    signal shift        : m32_5bits;
    signal shift_ext    : m32_word;
    signal ex_opcode    : m32_6bits;
    signal immediate    : m32_halfword;
    signal imm_ext      : m32_word;
    signal imm_ext_sht  : m32_word;
    signal j_imm        : m32_26bits;
 
    -- Control signals
    signal reg_dst      : m32_1bit;     -- Register destination
    signal alu_src      : m32_1bit;     -- Whether to use immediate or read
    signal mem_to_reg   : m32_1bit;     -- Whether to write memory to a register
    signal reg_write    : m32_1bit;     -- Write enable for register
    signal mem_read     : m32_1bit;     -- Whether to read from memory
    signal mem_write    : m32_1bit;     -- Whether to write to memory
    signal branch       : m32_2bits;     -- Whether to branch
    signal alu_op       : m32_2bits;    -- What the alu_op is
    signal jump         : m32_1bit;     -- Whether you should jump

    signal is_not_shift     : m32_1bit;
 
    signal pc_write     : m32_1bit := '1';
    signal if_id_write  : m32_1bit := '1';
    signal control_write: m32_1bit := '1';

    -- Other non-control signals connected to regfile
    signal wdata        : m32_word;     -- Register write data
    signal dst_reg      : m32_5bits;    -- regfile dest register

    signal forward_a    : m32_2bits;
    signal forward_b    : m32_2bits;
    signal alu_a        : m32_word;     -- Alu input 1
    signal alu_a_reg    : m32_word;
    signal alu_a_inter  : m32_word;
    signal alu_b_inter  : m32_word;     -- Alu input 2 intermediate, before muxed
    signal alu_b_reg    : m32_word;
    signal alu_b        : m32_word;     -- Alu input 2
    signal alu_ctrl     : m32_4bits;     -- Control signal for the alu
    signal alu_res      : m32_word;
    signal alu_zero     : m32_1bit;

    -- IF/ID pipeline output signals --

    signal IF_ID_inst   : m32_word;
    signal IF_ID_pc     : m32_word;

    -- ID/EX pipeline output signals --
    signal ID_EX_reg_write      : m32_1bit;
    signal ID_EX_mem_to_reg     : m32_1bit;
    signal ID_EX_branch         : m32_2bits;
    signal ID_EX_mem_read       : m32_1bit;
    signal ID_EX_mem_write      : m32_1bit;
    signal ID_EX_alu_src        : m32_1bit;
    signal ID_EX_alu_op         : m32_2bits;
    signal ID_EX_reg_dst        : m32_1bit;

    signal ID_EX_opcode         : m32_6bits;
    signal ID_EX_pc             : m32_word;
    signal ID_EX_rdata1         : m32_word;
    signal ID_EX_rdata2         : m32_word;
    signal ID_EX_imm_ext        : m32_word;
    signal ID_EX_rs             : m32_5bits;
    signal ID_EX_rt             : m32_5bits;
    signal ID_EX_rd             : m32_5bits;
	signal ID_EX_jump			: m32_1bit;
    signal ID_EX_j_imm          : m32_26bits;
	
	-- EX/MEM pipeline output signals --
	signal EX_MEM_reg_write		: m32_1bit;
	signal EX_MEM_mem_to_reg	: m32_1bit;
	signal EX_MEM_mem_write		: m32_1bit;
	signal EX_MEM_mem_read		: m32_1bit;
	signal EX_MEM_branch		: m32_2bits;
	signal EX_MEM_pc_plus_imm	: m32_word;
	signal EX_MEM_alu_zero		: m32_1bit;
	signal EX_MEM_alu_res		: m32_word;
	signal EX_MEM_rdata2		: m32_word;
	signal EX_MEM_dst_reg		: m32_5bits;
	signal EX_MEM_pc			: m32_word;
	signal EX_MEM_jump			: m32_1bit;
    signal EX_MEM_j_imm          : m32_26bits;
	
	-- MEM/WB pipeline output signals --
	signal MEM_WB_mem_to_reg	: m32_1bit;
	signal MEM_WB_reg_write		: m32_1bit;
	signal MEM_WB_alu_res		: m32_word;
	signal MEM_WB_rdata			: m32_word;
	signal MEM_WB_dst_reg		: m32_5bits;
 
    signal not_clock            : m32_1bit;

begin

    not_clock <= not clock;
	-- ID STAGE --
    imem_addr <= PC;
	
	-- MEM STAGE --
    dmem_addr <= EX_MEM_alu_res;
    dmem_read <= EX_MEM_mem_read;
    dmem_write <= EX_MEM_mem_write;
    dmem_wmask <= x"F";
    dmem_wdata <= EX_MEM_rdata2;

   
    -- Split the instructions into fields --
	-- ID STAGE --
    SPLIT : block
    begin
        opcode <= IF_ID_inst(31 downto 26);
        rs <= IF_ID_inst(25 downto 21);
        rt <= IF_ID_inst(20 downto 16);
        rd <= IF_ID_inst(15 downto 11);
        immediate <= IF_ID_inst(15 downto 0);
        j_imm <= IF_ID_inst(25 downto 0);
    end block;


    IF_ID_PIPELINE: IF_ID
        port map(
            i_inst      => inst,
            o_inst      => IF_ID_inst,
            i_pc        => pc_plus_4,
            o_pc        => IF_ID_pc,
            clock       => clock,
            reset       => reset,
            we          => if_id_write
        );

    ID_EX_PIPELINE: ID_EX
        port map(
            i_reg_write     => reg_write,
            o_reg_write     => ID_EX_reg_write,
            i_mem_to_reg    => mem_to_reg,
            o_mem_to_reg    => ID_EX_mem_to_reg,
            i_branch        => branch,
            o_branch        => ID_EX_branch,
            i_mem_read      => mem_read,
            o_mem_read      => ID_EX_mem_read,
            i_mem_write     => mem_write,
            o_mem_write     => ID_EX_mem_write,
            i_alu_src       => alu_src,
            o_alu_src       => ID_EX_alu_src,
            i_alu_op        => alu_op,
            o_alu_op        => ID_EX_alu_op,
            i_reg_dst       => reg_dst,
            o_reg_dst       => ID_EX_reg_dst,
            i_opcode        => opcode,
            o_opcode        => ID_EX_opcode,
            i_pc            => IF_ID_pc,
            o_pc            => ID_EX_pc,
            i_rdata1        => alu_a_inter,
            o_rdata1        => ID_EX_rdata1,
            i_rdata2        => alu_b_inter,
            o_rdata2        => ID_EX_rdata2,
            i_imm_ext       => imm_ext,
            o_imm_ext       => ID_EX_imm_ext,
            i_rs            => rs,
            o_rs            => ID_EX_rs,
            i_rt            => rt,
            o_rt            => ID_EX_rt,
            i_rd            => rd,
            o_rd            => ID_EX_rd,
			i_jump			=> jump,
			o_jump			=> ID_EX_jump,
            i_j_imm         => j_imm,
            o_j_imm         => ID_EX_j_imm,
            clock           => clock,
            reset       => reset
        );
		
	EX_MEM_PIPELINE: EX_MEM
		port map(
			-- Writeback signals from control (2) --
			i_mem_to_reg  => ID_EX_mem_to_reg,
			o_mem_to_reg  => EX_MEM_mem_to_reg,
			i_reg_write   => ID_EX_reg_write,
			o_reg_write   => EX_MEM_reg_write,
		-- MEM signals from control (3) --
			i_mem_write   => ID_EX_mem_write,
			o_mem_write   => EX_MEM_mem_write,
			i_mem_read    => ID_EX_mem_read,
			o_mem_read    => EX_MEM_mem_read,
			i_branch      => ID_EX_branch,
			o_branch      => EX_MEM_branch,
		-- Other MEM signals (5) --
			i_pc_plus_imm => pc_plus_imm,
			o_pc_plus_imm => EX_MEM_pc_plus_imm,
			i_alu_zero    => alu_zero,
			o_alu_zero    => EX_MEM_alu_zero,
			i_alu_res     => alu_res,
			o_alu_res     => EX_MEM_alu_res,
			i_rdata2      => alu_b_reg,
			o_rdata2      => EX_MEM_rdata2,
			i_dst_reg     => dst_reg,
			o_dst_reg     => EX_MEM_dst_reg,
			i_pc		  => ID_EX_pc,
			o_pc		  => EX_MEM_pc,
			i_jump		  => ID_EX_jump,
			o_jump		  => EX_MEM_jump,
            i_j_imm       => ID_EX_j_imm,
            o_j_imm       => EX_MEM_j_imm,
		-- Clock --
			clock         => clock,
            reset       => reset
		);
		
	MEM_WB_PIPELINE: MEM_WB
		port map(
			-- Writeback signals from control (2) --
			i_mem_to_reg  => EX_MEM_mem_to_reg,
			o_mem_to_reg  => MEM_WB_mem_to_reg,
			i_reg_write   => EX_MEM_reg_write,
			o_reg_write   => MEM_WB_reg_write,
			-- Other MEM signals (3) --
			i_alu_res     => EX_MEM_alu_res,
			o_alu_res     => MEM_WB_alu_res,
			i_rdata       => dmem_rdata,
			o_rdata       => MEM_WB_rdata,
			i_dst_reg     => EX_MEM_dst_reg,
			o_dst_reg     => MEM_WB_dst_reg,
			-- Clock --
			clock         => clock,
            reset       => reset
		);
        
	-- IF STAGE --
    PC_REG: reg
        port map(
            D       => pc_final,
            Q       => PC,
            WE      => pc_write,
            reset   => reset,
            clock   => clock
        );

	-- WB STAGE --
    WDATA_MUX: mux32bit2to1
        port map(
            one     => MEM_WB_alu_res,
            two     => MEM_WB_rdata,
            sel     => MEM_WB_mem_to_reg,
            output  => wdata
        );

    -- EX STAGE --
    DST_REG_MUX: mux5bit2to1
        port map(
            one     => ID_EX_rt,
            two     => ID_EX_rd,
            sel     => ID_EX_reg_dst,
            output  => dst_reg
        );
 
    -- ID STAGE --
    IMM_SIGN_EXTEND: extend16to32
        port map(
            input   => immediate,
            sign    => '1',
            output  => imm_ext
        );

    -- EX STAGE --
    SHIFT_ZERO_EXTEND: extend5to32
        port map(
            input   => shift,
            sign    => '0',
            output  => shift_ext
        );

 
    -- ID STAGE --
    CONTROL1: control
        port map(
            op_code     => opcode,
            reg_dst     => reg_dst,
            alu_src     => alu_src,
            mem_to_reg  => mem_to_reg,
            reg_write   => reg_write,
            mem_read    => mem_read,
            mem_write   => mem_write,
            branch      => branch,
            alu_op      => alu_op,
            jump        => jump,
            write => control_write
        );
 
    -- ID STAGE --
    REGFILE1: regfile
        port map(
            src1    => rs,
            src2    => rt,
            dst     => MEM_WB_dst_reg,
            wdata   => wdata,
            rdata1  => alu_a_inter,
            rdata2  => alu_b_inter,
            WE      => MEM_WB_reg_write,
            reset   => reset,
            clock   => not_clock
        );

    -- EX STAGE --        
    shift       <= ID_EX_imm_ext(10 downto 6);
    ex_opcode   <= ID_EX_imm_ext(5 downto 0);
 
    -- EX STAGE --
    ALUCONTROL1: aluControl
        port map(
            aluOP   => ID_EX_alu_op,
            funct   => ex_opcode,
            output  => alu_ctrl
        );

    -- FLOATING --
    HAZARD_DETECTION1: hazard_detection
        port map(
            id_ex_mem_read  => ID_EX_mem_read,
            if_id_rs        => rs,
            if_id_rt        => rt,
            id_ex_rt        => ID_EX_rt,
            pc_write        => pc_write,
            if_id_write     => if_id_write,
            control_write   => control_write
        );

    -- FLOATING --
    FORWARDING_UNIT1: forwarding_unit
        port map(
            id_ex_rs            => ID_EX_rs,
            id_ex_rt            => ID_EX_rt,
            ex_mem_rd           => EX_MEM_dst_reg,
            mem_wb_rd           => MEM_WB_dst_reg,
            ex_mem_reg_write    => EX_MEM_reg_write,
            mem_wb_reg_write    => MEM_WB_reg_write,
            forward_a           => forward_a,
            forward_b           => forward_b
        );
 
    -- EX STAGE --
    ALU_B_REG_MUX: mux32bit4to1
        port map(
            zero    => ID_EX_rdata2,
            one     => wdata,
            two     => EX_MEM_alu_res,
            three   => "00000000000000000000000000000000",
            sel     => forward_b,
            output  => alu_b_reg
        );

    -- EX STAGE --
    ALU_B_MUX: mux32bit2to1
        port map(
            one     => alu_b_reg,
            two     => ID_EX_imm_ext,
            sel     => ID_EX_alu_src,
            output  => alu_b
        );

    -- EX STAGE --
    is_not_shift <= or_reduce(ID_EX_opcode or ex_opcode);

    -- EX STAGE --
    ALU_A_REG_MUX: mux32bit4to1
        port map(
            zero    => ID_EX_rdata1,
            one     => wdata,
            two     => EX_MEM_alu_res,
            three   => "00000000000000000000000000000000",
            sel     => forward_a,
            output  => alu_a_reg
        );

    -- EX STAGE --
    ALU_A_MUX: mux32bit2to1
        port map(
            one     => shift_ext,
            two     => alu_a_reg,
            sel     => is_not_shift,
            output  => alu_a
        );

	-- EX STAGE --	
    ALU1: ALU
        port map(
            rdata1      => alu_a,
            rdata2      => alu_b,
            alu_code    => alu_ctrl,
            result      => alu_res,
            zero        => alu_zero
        );

    -- PCSrc --
	-- MEM STAGE --
    take_branch <= ((EX_MEM_branch(1) and not EX_MEM_branch(0) and EX_MEM_alu_zero) or (EX_MEM_branch(1) and EX_MEM_branch(0) and not EX_MEM_alu_zero));
    
    -- IF STAGE --
    PC_INC_ADD: adder
        port map(
            src1        => PC,
            src2        => "00000000000000000000000000000100",
            result      => pc_plus_4
        );

    -- EX STAGE --
    IMM_SHIFT: shift32
        port map(
            input   => ID_EX_imm_ext,
            output  => imm_ext_sht
        );

    -- EX STAGE --
    PC_BRANCH_ADD: adder
        port map(
            src1        => ID_EX_pc,
            src2        => imm_ext_sht,
            result      => pc_plus_imm
        );

    -- MEM STAGE --
    PC_BRANCH_MUX: mux32bit2to1
        port map(
            one         => pc_plus_4,
            two         => EX_MEM_pc_plus_imm,
            sel         => take_branch,
            output      => pc_branch
        );
		
	    -- Jump target
    j_target <= EX_MEM_pc(31 downto 28) & EX_MEM_j_imm & "00";
	
    PC_JUMP_MUX: mux32bit2to1
        port map(
            one         => pc_branch,
            two         => j_target,
            sel         => EX_MEM_jump,
            output      => pc_final
        );
 
end structure;
