library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.mips32.all;

entity forwarding_unit is
    port(id_ex_rs           : in m32_5bits;
         id_ex_rt           : in m32_5bits;
         ex_mem_rd          : in m32_5bits;
         mem_wb_rd          : in m32_5bits;
         ex_mem_reg_write   : in m32_1bit;
         mem_wb_reg_write   : in m32_1bit;
         forward_a          : out m32_2bits;
         forward_b          : out m32_2bits);
end entity;

architecture behavior of forwarding_unit is
begin
    forward_a <= "10" when ((ex_mem_reg_write = '1') and (ex_mem_rd /= "00000") and (ex_mem_rd = id_ex_rs)) else    -- ex hazard
                 "01" when ((mem_wb_reg_write = '1') and (mem_wb_rd /= "00000") and (mem_wb_rd = id_ex_rs) and not ((ex_mem_reg_write = '1') and (ex_mem_rd /= "00000") and (ex_mem_rd = id_ex_rs))) else    -- mem hazard
                 "00";      -- no hazard

    forward_b <= "10" when ((ex_mem_reg_write = '1') and (ex_mem_rd /= "00000") and (ex_mem_rd = id_ex_rt)) else    -- ex hazard
                 "01" when ((mem_wb_reg_write = '1') and (mem_wb_rd /= "00000") and (mem_wb_rd = id_ex_rt) and not ((ex_mem_reg_write = '1') and (ex_mem_rd /= "00000") and (ex_mem_rd = id_ex_rt))) else    -- mem hazard
                 "00";      -- no hazard
end behavior;
