library IEEE;
use IEEE.std_logic_1164.all;
use work.mips32.all;

entity mux5bit2to1 is
  port(one     : in m32_5bits; --if sel is 0
       two     : in m32_5bits; --if sel is 1
       sel     : in m32_1bit;
       output  : out m32_5bits);

end mux5bit2to1;

architecture dataflow of mux5bit2to1 is

begin

with sel select output <= one when '0', two when others;   

end dataflow;
