-- EX / MEM Pipeline
-- Finished

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_misc.all;
use work.mips32.all;
 
entity MEM_WB is
  port (
    -- Writeback signals from control (2) --
        i_mem_to_reg  : in  m32_1bit;     -- Input  WB #1
        o_mem_to_reg  : out m32_1bit;     -- Output WB #1
        i_reg_write   : in  m32_1bit;     -- Input  WB #2
        o_reg_write   : out m32_1bit;     -- Output WB #2
    -- Other MEM signals (3) --
        i_alu_res     : in  m32_word;
        o_alu_res     : out m32_word;
        i_rdata       : in  m32_word;
        o_rdata       : out m32_word;
        i_dst_reg     : in  m32_5bits;
        o_dst_reg     : out m32_5bits;
    -- Clock --
        clock         : in  m32_1bit;
        reset         : in  m32_1bit);
end MEM_WB;
 
architecture structure of MEM_WB is

    component reg is
        generic (M : integer := 32);                  -- Size of the register
        port(D      : in  m32_vector(M-1 downto 0);  -- Data input
             Q      : out m32_vector(M-1 downto 0);  -- Data output
             WE     : in  m32_1bit;                  -- Write enableenable
             reset  : in  m32_1bit;                  -- The clock signal
             clock  : in  m32_1bit);                 -- The reset signal
    end component;

    component reg_1bit is
        port(D      : in  m32_1bit;  -- Data input
             Q      : out m32_1bit;  -- Data output
             WE     : in  m32_1bit;                  -- Write enableenable
             reset  : in  m32_1bit;                  -- The clock signal
             clock  : in  m32_1bit);                 -- The reset signal
    end component;

begin

    mem_to_reg_reg: reg_1bit
        port map(
            D		=> i_mem_to_reg,
            Q		=> o_mem_to_reg,
            WE		=> '1',
            reset	=> reset,
            clock	=> clock
        );
    reg_write_reg: reg_1bit
        port map(
            D		=> i_reg_write,
            Q		=> o_reg_write,
            WE		=> '1',
            reset	=> reset,
            clock	=> clock
        );
    alu_res_reg: reg generic map(32)
        port map(
            D		=> i_alu_res,
            Q		=> o_alu_res,
            WE		=> '1',
            reset	=> reset,
            clock	=> clock
        );
    rdata2_reg: reg generic map(32)
        port map(
            D		=> i_rdata,
            Q		=> o_rdata,
            WE		=> '1',
            reset	=> reset,
            clock	=> clock
        );
    dst_reg_reg: reg generic map(5)
        port map(
            D		=> i_dst_reg,
            Q		=> o_dst_reg,
            WE		=> '1',
            reset	=> reset,
            clock	=> clock
        );
 
end structure;


