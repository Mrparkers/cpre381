library IEEE;
use IEEE.std_logic_1164.all;
use work.mips32.all;

entity mux32bit4to1 is
  port(zero     : in m32_word; --if sel is 00
       one      : in m32_word; --if sel is 01
       two      : in m32_word; -- if sel is 10
       three    : in m32_word; -- if sel is 11
       sel      : in m32_2bits;
       output   : out m32_word);

end mux32bit4to1;

architecture dataflow of mux32bit4to1 is

begin
    with sel select output <= zero when "00",
        one when "01",
        two when "10",
        three when others;

end dataflow;
