-- EX / MEM Pipeline
-- Finished

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_misc.all;
use work.mips32.all;
 
entity EX_MEM is
  port (
    -- Writeback signals from control (2) --
        i_mem_to_reg  : in  m32_1bit;     -- Input  WB #1
        o_mem_to_reg  : out m32_1bit;     -- Output WB #1
        i_reg_write   : in  m32_1bit;     -- Input  WB #2
        o_reg_write   : out m32_1bit;     -- Output WB #2
    -- MEM signals from control (3) --
        i_mem_write   : in  m32_1bit;     -- Input  MEM #1
        o_mem_write   : out m32_1bit;     -- Output MEM #1
        i_mem_read    : in  m32_1bit;     -- Input  MEM #2
        o_mem_read    : out m32_1bit;     -- Output MEM #2
        i_branch      : in  m32_2bits;    -- Input  MEM #3
        o_branch      : out m32_2bits;    -- Output MEM #3
    -- Other MEM signals (5) --
        i_pc_plus_imm : in  m32_word;     -- Input  Other #1
        o_pc_plus_imm : out m32_word;     -- Output Other #1
        i_alu_zero    : in  m32_1bit;
        o_alu_zero    : out m32_1bit;
        i_alu_res     : in  m32_word;
        o_alu_res     : out m32_word;
        i_rdata2      : in  m32_word;
        o_rdata2      : out m32_word;
        i_dst_reg     : in  m32_5bits;
        o_dst_reg     : out m32_5bits;
		i_pc		  : in  m32_word;
		o_pc		  : out m32_word;
		i_jump		  : in m32_1bit;
		o_jump		  : out m32_1bit;
		i_j_imm       : in m32_26bits;
        o_j_imm       : out m32_26bits;
    -- Clock --
        clock         : in  m32_1bit;
        reset         : in  m32_1bit);    -- System clock
end EX_MEM;
 
architecture structure of EX_MEM is

    component reg is
        generic (M : integer := 32);                  -- Size of the register
        port(D      : in  m32_vector(M-1 downto 0);  -- Data input
             Q      : out m32_vector(M-1 downto 0);  -- Data output
             WE     : in  m32_1bit;                  -- Write enableenable
             reset  : in  m32_1bit;                  -- The clock signal
             clock  : in  m32_1bit);                 -- The reset signal
    end component;

    component reg_1bit is
        port(D      : in  m32_1bit;  -- Data input
             Q      : out m32_1bit;  -- Data output
             WE     : in  m32_1bit;                  -- Write enableenable
             reset  : in  m32_1bit;                  -- The clock signal
             clock  : in  m32_1bit);                 -- The reset signal
    end component;

begin

    mem_to_reg_reg: reg_1bit
        port map(
            D		=> i_mem_to_reg,
            Q		=> o_mem_to_reg,
            WE		=> '1',
            reset	=> reset,
            clock	=> clock
        );
    reg_write_reg: reg_1bit
        port map(
            D		=> i_reg_write,
            Q		=> o_reg_write,
            WE		=> '1',
            reset	=> reset,
            clock	=> clock
        );
    mem_write_reg: reg_1bit
        port map(
            D		=> i_mem_write,
            Q		=> o_mem_write,
            WE		=> '1',
            reset	=> reset,
            clock	=> clock
        );
    mem_read_reg: reg_1bit
        port map(
            D		=> i_mem_read,
            Q		=> o_mem_read,
            WE		=> '1',
            reset	=> reset,
            clock	=> clock
        );
    branch_reg: reg generic map(2)
        port map(
            D		=> i_branch,
            Q		=> o_branch,
            WE		=> '1',
            reset	=> reset,
            clock	=> clock
        );
    pc_plus_imm_reg: reg generic map(32)
        port map(
            D		=> i_pc_plus_imm,
            Q		=> o_pc_plus_imm,
            WE		=> '1',
            reset	=> reset,
            clock	=> clock
        );
    alu_zero_reg: reg_1bit
        port map(
            D		=> i_alu_zero,
            Q		=> o_alu_zero,
            WE		=> '1',
            reset	=> reset,
            clock	=> clock
        );
    alu_res_reg: reg generic map(32)
        port map(
            D		=> i_alu_res,
            Q		=> o_alu_res,
            WE		=> '1',
            reset	=> reset,
            clock	=> clock
        );
    rdata2_reg: reg generic map(32)
        port map(
            D		=> i_rdata2,
            Q		=> o_rdata2,
            WE		=> '1',
            reset	=> reset,
            clock	=> clock
        );
    dst_reg_reg: reg generic map(5)
        port map(
            D		=> i_dst_reg,
            Q		=> o_dst_reg,
            WE		=> '1',
            reset	=> reset,
            clock	=> clock
        );
    pc_reg: reg generic map(32)
        port map(
            D		=> i_pc,
            Q		=> o_pc,
            WE		=> '1',
            reset	=> reset,
            clock	=> clock
        );	
    jump_reg: reg_1bit
        port map(
            D		=> i_jump,
            Q		=> o_jump,
            WE		=> '1',
            reset	=> reset,
            clock	=> clock
        );

    j_imm_reg: reg generic map(26)
        port map(
            D       => i_j_imm,
            Q       => o_j_imm,
            WE      => '1',
            reset   => reset,
            clock   => clock
        );
end structure;


