library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.mips32.all;

entity aluControl is
    port(aluOP: in m32_2bits;
         funct: in m32_6bits;
         output: out m32_4bits);
end aluControl;

architecture mixed of aluControl is

begin
    output <= "0010" when aluOP = "00" else --add
    "0110" when aluOP = "01" else --subtract
    "0010" when aluOP = "10" and funct = "100000" else --add
    "0110" when aluOP = "10" and funct = "100010" else --subtract
    "0000" when aluOP = "10" and funct = "100100" else --and
    "0001" when aluOP = "10" and funct = "100101" else --or
    "0111" when aluOP = "10" and funct = "101010" else --slt
    "1110" when aluOP = "10" and funct = "000000" else
    "1111"; --default
  
end mixed;
