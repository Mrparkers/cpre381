library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.mips32.all;

entity hazard_detection is
    port(id_ex_mem_read     : in m32_1bit;
         if_id_rs           : in m32_5bits;
         if_id_rt           : in m32_5bits;
         id_ex_rt           : in m32_5bits;
         id_branch          : in m32_2bits;
         ex_branch          : in m32_2bits;
         mem_branch         : in m32_2bits;
         pc_write           : out m32_1bit;
         if_id_write        : out m32_1bit;
         control_write      : out m32_1bit);
end entity;

architecture behavior of hazard_detection is
begin

    pc_write <= '0' when ((id_ex_mem_read = '1') and ((id_ex_rt = if_id_rs) or (id_ex_rt = if_id_rt)))  -- data hazard
                else '0' when (id_branch(0) = '1' or id_branch(1) = '1' or ex_branch(0) = '1' or ex_branch(1) = '1' or mem_branch(0) = '1' or mem_branch(1) = '1') -- control hazard
                else '1';
    if_id_write <= '0' when ((id_ex_mem_read = '1') and ((id_ex_rt = if_id_rs) or (id_ex_rt = if_id_rt)))  -- data hazard
                   else '0' when (id_branch(0) = '1' or id_branch(1) = '1' or ex_branch(0) = '1' or ex_branch(1) = '1' or mem_branch(0) = '1' or mem_branch(1) = '1') -- control hazard
                    else '1';
    control_write <= '0' when ((id_ex_mem_read = '1') and ((id_ex_rt = if_id_rs) or (id_ex_rt = if_id_rt)))  -- data hazard
                     else '0' when (id_branch(0) = '1' or id_branch(1) = '1' or ex_branch(0) = '1' or ex_branch(1) = '1' or mem_branch(0) = '1' or mem_branch(1) = '1')
                      else '1';


end behavior;
