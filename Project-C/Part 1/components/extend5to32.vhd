library IEEE;
use IEEE.std_logic_1164.all;
use work.mips32.all;

entity extend5to32 is
  port(input     : in m32_5bits;
       sign      : in m32_1bit;
       output    : out m32_word);

end extend5to32;

architecture dataflow of extend5to32 is

begin
process (input, sign)
begin
if (sign = '0') then
	--o_32 <= std_logic_vector(resize(unsigned(i_8), o_32'width));
	output(4 downto 0) <= input;
	output(31 downto 5) <= (others => '0');
else
	output(4 downto 0) <= input;
	output(31 downto 5) <= (others => input(4));
end if;
end process;  

end dataflow;
