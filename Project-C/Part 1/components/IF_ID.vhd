

-- cpu.vhd: Single-cycle implementation of MIPS32 for CprE 381, fall 2013,
-- Iowa State University
--
-- Zhao Zhang, fall 2013
 
-- The CPU entity. It connects to 1) an instruction memory, 2) a data memory, and
-- 3) an external clock source.
--
-- Note: This is a partical sample
--
 
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_misc.all;
use work.mips32.all;
 
entity IF_ID is
  port (i_inst      : in m32_word;
        o_inst      : out m32_word;
        i_pc        : in m32_word;
        o_pc        : out m32_word;
        clock       : in m32_1bit;
        reset         : in  m32_1bit);    -- System clock
end IF_ID;
 
-- This architecture of CPU must be dominantly structural, with no bahavior
-- modeling, and only data flow statements to copy/split/merge signals or
-- with a single level of basic logic gates.
architecture structure of IF_ID is

    component reg is
        generic (M : integer := 32);                  -- Size of the register
        port(D      : in  m32_vector(M-1 downto 0);  -- Data input
             Q      : out m32_vector(M-1 downto 0);  -- Data output
             WE     : in  m32_1bit;                  -- Write enableenable
             reset  : in  m32_1bit;                  -- The clock signal
             clock  : in  m32_1bit);                 -- The reset signal
    end component;
 
begin

    INST_REG: reg
        port map(
            D       => i_inst,
            Q       => o_inst,
            WE      => '1',
            reset   => reset,
            clock   => clock
        );

    PC_REG: reg
        port map(
            D       => i_pc,
            Q       => o_pc,
            WE      => '1',
            reset   => reset,
            clock   => clock
        );


end structure;


