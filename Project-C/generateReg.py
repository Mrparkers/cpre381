signals = ('mem_to_reg','reg_write','mem_write','mem_read','branch','pc_plus_imm','alu_zero','alu_res','rdata2','dst_reg')

for signal in signals:
	print signal + "_reg: reg generic map(32)"
	print "    port map("
	print "        D\t\t=> i_" + signal + ","
	print "        Q\t\t=> o_" + signal + ","
	print "        WE\t\t=> '1',"
	print "        reset\t=> '0',"
	print "        clock\t=> clock"
	print "    );"


