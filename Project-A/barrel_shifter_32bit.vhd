library IEEE;
use IEEE.std_logic_1164.all;
use work.util.all;

entity barrelShifter is
  port(i_input : in std_logic_vector(31 downto 0);
       amtShift : in std_logic_vector(4 downto 0);
       sign : in std_logic;
       i_left : in std_logic;
       o_f : out std_logic_vector(31 downto 0));
end barrelShifter;

architecture shifter of barrelShifter is
  
  component mux32to1bit
    port(sel: in std_logic_vector(4 downto 0);
         input: in std_logic_vector(31 downto 0);
         output: out std_logic);
  end component;
  
  component mux2to1bit
    port(A  : in std_logic;
         B  : in std_logic;
         sel  : in std_logic;
         output  : out std_logic);
    end component;
  
  signal right, left : std_logic_vector(31 downto 0);
  signal muxRightS, muxLeftS : array32_bit(31 downto 0);
  
  begin
    
    L1: for i in 0 to 31 generate --iterate through each mux
      L2: for j in 0 to 31-i generate --iterate through each input for this mux
        muxRightS(i)(j) <= i_input(i+j);
      end generate;
      
      L3: for j in 32-i to 31 generate
        muxRightS(i)(j) <= sign and i_input(31);
      end generate;
      
      L4: for j in 0 to i generate
        muxLeftS(i)(j) <= i_input(i-j);
      end generate;
      
      L5: for j in i+1 to 31 generate
        muxLeftS(i)(j) <= '0';
      end generate;
      
      MUXRIGHT: mux32to1bit
        port map(sel => amtShift,
		 input => muxRightS(i),
                 output => right(i));
                 
      MUXLEFT: mux32to1bit
        port map(sel => amtShift,
		 input => muxLeftS(i),
                 output => left(i));
                 
      MUX: mux2to1bit
        port map(A => right(i),
                 B => left(i),
                 sel => i_left,
                 output => o_f(i));
    end generate;

end shifter;
