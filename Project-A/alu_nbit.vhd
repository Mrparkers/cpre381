 library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_misc.all;
use IEEE.NUMERIC_STD.ALL;

entity alu_nbit is
port(   Clk : in std_logic; --clock signal
        A,B : in std_logic_vector(31 downto 0); --input operands
        ctrl : in std_logic_vector(3 downto 0); --Operation to be performed
        F : out std_logic_vector(31 downto 0);  --output of ALU
        zero: out std_logic;
        carry_out: out std_logic;
        overflow: out std_logic
        );
end alu_nbit;

architecture Behavioral of alu_nbit is

--temporary signal declaration.
signal Reg1,Reg2 : std_logic_vector(32 downto 0);
signal Reg3 : std_logic_vector(32 downto 0);

begin

Reg1 <= '0' & A;
Reg2 <= '0' & B;
F <= Reg3(31 downto 0);

process(Clk)
begin

    if(rising_edge(Clk)) then --Do the calculation at the positive edge of clock cycle.
        case ctrl is
            -- addition
            -- unsigned
            when "0000" =>
                Reg3 <= std_logic_vector(unsigned(Reg1) + unsigned(Reg2));
            -- signed
            when "0001" =>
                Reg3 <= std_logic_vector(signed(Reg1) + signed(Reg2));
            -- subtraction
            -- unsigned
            when "0010" =>
                Reg3 <= std_logic_vector(unsigned(Reg1) - unsigned(Reg2));
            -- signed
            when "0011" =>
                Reg3 <= std_logic_vector(signed(Reg1) - signed(Reg2));
            -- slt
            -- unsigned
            when "0100" =>
                if (unsigned(Reg1) < unsigned(Reg2)) then
                    Reg3 <= '0' & x"00000001";
                else
                    Reg3 <= '0' & x"00000000";
                end if;
            -- signed
            when "0101" =>
                if (signed(Reg1) < signed(Reg2)) then
                    Reg3 <= '0' & x"00000001";
                else
                    Reg3 <= '0' & x"00000000";
                end if;
            when "0110" =>
                Reg3 <= (Reg1 nand Reg2); --NAND gate
            when "0111" =>
                Reg3 <= (Reg1 nor Reg2); --NOR gate              
            when "1000" =>
                Reg3 <= (Reg1 and Reg2);  --AND gate
            when "1001" =>
                Reg3 <= (Reg1 or Reg2);  --OR gate   
            when "1010" =>
                Reg3 <= (Reg1 xor Reg2); --XOR gate
            when others =>
                NULL;
        end case;

        if(ctrl = "0100" or ctrl = "0101" or ctrl = "0110" or ctrl = "0111" or ctrl = "1000" or ctrl = "1001" or ctrl = "1010") then
            carry_out <= '0';
            overflow <= '0';
        -- unsigned addtion/sub
        elsif(ctrl = "0000" or ctrl = "0010") then
            carry_out <= Reg3(32);
            overflow <= Reg3(32);
        -- signed addtion/sub
        elsif(ctrl = "0001" or ctrl = "0011") then
            carry_out <= Reg3(32);
            if(Reg1(31) = Reg2(31)) then
                overflow <= not(Reg3(32) xor Reg1(31));
            else
                overflow <= '0';
            end if;
        end if;

        zero <= nor_reduce(Reg3(31 downto 0));

    end if;
   
end process;   

end Behavioral;


