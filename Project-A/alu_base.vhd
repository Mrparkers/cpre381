library IEEE;
use IEEE.std_logic_1164.all;

entity alu_base is

    port(control: in std_logic_vector(3 downto 0);
         input1: in std_logic;
         input2: in std_logic;
         carry_in: in std_logic;
         result: out std_logic;
         carry_out: out std_logic);

end alu_base;

architecture arch of alu_base is

begin

    process(control, input1, input2, carry_in) begin
        -- NOR
        if(control = b"0000") then
            result <= (input1 nor input2);
            carry_out <= '0';
        -- NAND
        elsif(control = b"0001") then
            result <= (input1 nand input2);
            carry_out <= '0';
        -- XOR
        elsif(control = b"0010") then
            result <= (input1 xor input2);
            carry_out <= '0';
        -- OR
        elsif(control = b"0011") then
            result <= (input1 or input2);
            carry_out <= '0';
        -- AND
        elsif(control = b"0100") then
            result <= (input1 and input2);
            carry_out <= '0';
        -- SLT
        elsif(control = b"0101") then
            result <= (not input1 and input2);
            carry_out <= '0';
        -- ADD/SUB
        elsif(control = b"0110") then
            result <= (carry_in xor input1 xor input2);
            carry_out <= ((carry_in and input1) or (carry_in and input2) or (input1 and input2));
        end if;

    end process;

end arch;


