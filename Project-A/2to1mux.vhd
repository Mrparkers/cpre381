library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity mux2to1bit is
    port(A  : in std_logic;
         B  : in std_logic;
         sel  : in std_logic;
         output  : out std_logic);
end mux2to1bit;

architecture mixed of mux2to1bit is

begin
    output <= A when sel = '0' else B;

end mixed;
