library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity mux32to1bit is
    port(sel: in std_logic_vector(4 downto 0);
         input: in std_logic_vector(31 downto 0);
         output: out std_logic);
end mux32to1bit;

architecture mixed of mux32to1bit is

begin
    output <= input(0) when sel = "00000" else
    input(1) when sel = "00001" else
    input(2) when sel = "00010" else
    input(3) when sel = "00011" else
    input(4) when sel = "00100" else
    input(5) when sel = "00101" else
    input(6) when sel = "00110" else
    input(7) when sel = "00111" else
    input(8) when sel = "01000" else
    input(9) when sel = "01001" else
    input(10) when sel = "01010" else
    input(11) when sel = "01011" else
    input(12) when sel = "01100" else
    input(13) when sel = "01101" else
    input(14) when sel = "01110" else
    input(15) when sel = "01111" else
    input(16) when sel = "10000" else
    input(17) when sel = "10001" else
    input(18) when sel = "10010" else
    input(19) when sel = "10011" else
    input(20) when sel = "10100" else
    input(21) when sel = "10101" else
    input(22) when sel = "10110" else
    input(23) when sel = "10111" else
    input(24) when sel = "11000" else
    input(25) when sel = "11001" else
    input(26) when sel = "11010" else
    input(27) when sel = "11011" else
    input(28) when sel = "11100" else
    input(29) when sel = "11101" else
    input(30) when sel = "11110" else
    input(31);
  
end mixed;
